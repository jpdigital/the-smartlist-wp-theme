<form role="search" method="get" class="search-form" action="<?php echo get_option('home'); ?>/">

	<div class="form-body">
	
		<fieldset class="row">
			<label class="medium-6 large-4 columns">
				<span class="screen-reader-text">Search for</span>
				<input type="search" class="search-field" placeholder="Search" value="<?php the_search_query(); ?>" name="s" title="Search for" />
			</label>

			<label class="medium-6 large-4 columns">
				<span class="screen-reader-text">Location</span>
				<input type="search" class="search-field" placeholder="Location" value="<?php if(isset($_GET['location'])) : echo($_GET['location']); endif; ?>" name="location" title="Location" />
			</label>

			<label class="medium-12 large-4 columns select-wrap">
				<span class="screen-reader-text">Category/Sector</span>
				<?php build_select('job_cat', 'Category/Sector'); ?>
			</label>
		</fieldset>
<!-- 
		<fieldset class="row">

			<fieldset class="large-4 columns">
				<legend>Salary <span class="info">Use increments of 1000</span></legend>
				<div class="row">
					<div class="medium-6 large-6 columns">
						<input type="number" name="salary_min" value="<?php if(isset($_GET['salary_min'])) : echo($_GET['salary_min']); else : echo('0'); endif; ?>" min="0" max="999000" step="1000" />
					</div>

					<div class="medium-6 large-6 columns">
						<input type="number" name="salary_max" value="<?php if(isset($_GET['salary_max'])) : echo($_GET['salary_max']); else : echo('100000'); endif; ?>" min="0" max="999000" step="1000" />
					</div>
				</div>
			</fieldset>

			<fieldset class="large-8 columns checkboxes">
				<legend>Job type</legend>
				<div class="row collapse-large">
					<?php build_checkboxes('job_type', 'job_type'); ?>
				</div>
			</fieldset>
	
		</fieldset>
 -->
	</div><?php /* form-body */ ?>

	<footer>
		<button type="submit" class="search-submit">Search</button>
    <a href="/cv/" class="button-request-callback submit-your-cv-link">
      Submit your CV
    </a>
	</footer>

</form>