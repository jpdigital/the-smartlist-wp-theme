<?php get_header(); ?>

	<?php if (have_posts()) : while (have_posts()) : the_post(); ?>

  <?php
    $slide_1_active = get_field('slide_1_active');
    $slide_2_active = get_field('slide_2_active');
    
    if( $slide_1_active ) {
        $slide_1_image = get_field('slide_1_image');
        $slide_1_bg = get_field('slide_1_background_color');
        $slide_1_header_text = get_field('slide_1_header_text');
        $slide_1_text = get_field('slide_1_text');
        $slide_1_button_text = get_field('slide_1_button_text');
        $slide_1_button_link = get_field('slide_1_button_link');
    }
    
    if( $slide_2_active ) {
        $slide_2_image = get_field('slide_2_image');
        $slide_2_bg = get_field('slide_2_background_color');
        $slide_2_header_text = get_field('slide_2_header_text');
        $slide_2_text = get_field('slide_2_text');
        $slide_2_play_button_link = get_field('slide_2_play_button_link');
    }
  ?>

    <section class="home-intro">
      <div id="home-banner-carousel">
    
        <?php if( $slide_1_active ) : ?>
          <div class="slider-item hero-media" style="background-image: url(<?php echo $slide_1_image; ?>); <?php if($slide_1_bg) : ?>background-color:<?php echo $slide_1_bg; ?>;<?php endif; ?>">  
            <div class="row copy center">
              <h1 class="caps"><?php echo $slide_1_header_text; ?></h1>
              <h3><?php echo $slide_1_text; ?></h3>
              <a href="<?php echo $slide_1_button_link; ?>"><button class="button-request-callback button-find-job"><img src="/wp-content/uploads/2015/09/search-icon.png" width="12" alt="" /> <?php echo $slide_1_button_text; ?></button></a>  
            </div>
          </div>
       <?php endif; ?>
       
       <?php if( $slide_2_active ) : ?>
          <div class="slider-item hero-media" style="background-image: url(<?php echo $slide_2_image; ?>); <?php if($slide_2_bg) : ?>background-color:<?php echo $slide_2_bg; ?>;<?php endif; ?>">
            <div class="row copy center">
              <h1 class="caps"><?php echo $slide_2_header_text; ?></h1>
              <h3><?php echo $slide_2_text; ?></h3>
              <a class="play fancybox-media" href="" data-src="<?php echo $slide_2_play_button_link; ?>"><img src="<?php echo get_stylesheet_directory_uri(); ?>/library/images/general/play.png" alt="Play"></a>
            </div>
          </div>
        <?php endif; ?>
                  
      </div>
    </section>


		<section class="service-intros row">

			<div class="small-12 columns">
				<?php if(get_field('recruitment_panels_title')) : ?>
					<h1 class="static-border static-border-large"><?php the_field('recruitment_panels_title'); ?></h1>
				<?php endif; ?>
			</div>
			<div class="clear"></div>


			<?php if( have_rows('recruitment_panels') ): $article_count=0; ?>
				<?php while( have_rows('recruitment_panels') ): the_row(); $article_count++; ?>

					<article class="small-12 medium-4 columns article-<?php echo $article_count; ?>">

						<header>
								<div class="service-image">
									<a href="<?php the_sub_field('link_url'); ?>">
										<?php $image = wp_get_attachment_image_src(get_sub_field('image'), 'thumbnail'); ?>
										<?php if($image) : ?><img src="<?php echo $image[0]; ?>" alt="<?php echo get_the_title(get_field('image')) ?>" /><?php else: ?>[No image selected]<?php endif; ?>
									</a>
								</div>

								<div class="request-callback-animated">
									<a href="<?php the_sub_field('link_url'); ?>#callback" class="desktop">
										<img src="<?php echo get_stylesheet_directory_uri(); ?>/library/images/general/phone-white-line.png" class="" alt="Phone" width="12">
										<p>Request a callback</p>
									</a>

									<a href="tel:<?php the_field('tel_eng','options'); ?>" class="mobile">
										<img src="<?php echo get_stylesheet_directory_uri(); ?>/library/images/general/phone-white-line.png" class="" alt="Phone" width="12">
										<p>Call us</p>
									</a>
								</div>

								<?php if(get_sub_field('title')) : ?><a href="<?php the_sub_field('link_url'); ?>"><h2><?php the_sub_field('title'); ?></h2></a><?php endif; ?>
						</header>

						<div class="content">
							<?php if(get_sub_field('excerpt')) : ?><p><?php the_sub_field('excerpt'); ?></p><?php endif; ?>
						</div>
						<?php if(get_sub_field('link_url')) : ?><a href="<?php the_sub_field('link_url'); ?>"><button class="learn-more"><?php the_sub_field('link_text'); ?></button></a><?php endif; ?>
					</article>

				<?php endwhile; ?>
			<?php endif; ?>
		</section><?php /* service-intros */ ?>


		<section class="customer-service-video" style="background-image: url('<?php the_field('customer_service_video_poster_frame'); ?>')">
			<div class="overlay">
				<div class="text">
					<h3>Meet your team</h3>
					<p>We're proud of our dedication to customer service. That's why over 3000 businesses have trusted us to fill vacancies, and over 93% of our clients rate us as exceptional.</p>
				</div>

				<div class="trustpilot-logo">
					<div class="tp_-_category-badge" data-tp-settings="domainId:8191446,size:100">
						<a href="https://uk.trustpilot.com/review/www.thesmartlist.co.uk" hidden>The Smart List Reviews</a>
					</div>
					<script type="text/javascript">
					 // (function(){ var a = "https:" == document.location.protocol ? "https://s.trustpilot.com" : "http://s.trustpilot.com", b=document.createElement("script");b.type="text/javascript";b.async=true;b.src=a+"/tpelements/tp_elements_all.js";var c=document.getElementsByTagName("script")[0];c.parentNode.insertBefore(b,c)})();
					</script>
				</div>
			</div>

			<video id="customerService" width="100%" muted="true" loop="" poster="<?php the_field('customer_service_video_poster_frame'); ?>">
     		<source src="<?php the_field('customer_service_video_mp4'); ?>" type="video/mp4">
				<source src="<?php the_field('customer_service_video_ogg'); ?>" type="video/ogg">
   			Oh no! Your browser doesn't support the video tag.
  		</video>

  		<div class="arrow left-arrow"></div>
  		<div class="arrow right-arrow"></div>
		</section><?php /* customer-service-video */ ?>


		<?php get_template_part( 'partials/content', 'trustpilot' ); ?>


		<div class="news-download">

			<div class="row">

				<div class="small-12 large-4 columns latest-insights">
					<div class="center header">
						<h1 class="static-border static-border-large">Latest Insights</h1>
					</div>
					<?php $pcat = get_field('insights_post_cat'); //This returns an array so refenrence array position 0 when used! ?>

					<?php $args = array(
						'cat'      => $pcat[0],
						'orderby'    => 'DESC',
						'posts_per_page' => 1,
						'category__not_in' => 9
					); query_posts($args); ?>

					<?php if (have_posts()) : ?>

						<section>
							<?php while (have_posts()) : the_post(); ?>

								<article>
									<?php
										if ( has_post_thumbnail() ) :

											the_post_thumbnail();

										else : ?>

											<img src="<?php echo get_stylesheet_directory_uri(); ?>/library/images/general/insight-placeholder.png" class="" alt="Insights">

										<?php endif;
									?>


									<a href="<?php the_permalink(); ?>"><h2><?php the_title(); ?></h2></a>
									<time datetime="<?php the_time( 'Y-m-d' ); ?>" pubdate><?php the_time('jS F Y'); ?></time>
									<?php the_excerpt(); ?>
									<a href="<?php the_permalink(); ?>"><button>Read more</button></a>
								</article>

							<?php endwhile; ?>
						</section>

					<?php endif; ?>

					<?php wp_reset_query(); ?>
				</div>



				<div class="small-12 large-7 large-offset-1 columns">
					<section>
						<?php
							$form = get_field('form_to_display');
							gravity_form_enqueue_scripts($form->id, true);
							gravity_form($form->id, true, false, false, '', true, 1);
						?>
					</section>
				</div>


			</div><?php /* row */ ?>
		</div><?php /* news-download */ ?>


	<?php endwhile; endif; ?>

<?php get_footer();
