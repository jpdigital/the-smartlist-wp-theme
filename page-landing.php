<?php /* Template Name: Landing Page */ ?>

<?php get_header(); ?>

	<?php get_template_part('partials/content-breadcrumb'); ?>	

	<?php if (have_posts()) : while (have_posts()) : the_post(); ?>

		<div class="row">
			<div class="small-12 large-6 large-offset-6 columns force-right">

				<header class="article-header">
					<?php 
					$image = get_field('banner_image');
					if($image): ?>
						<img src="<?php echo $image['url']; ?>" alt="<?php echo $image['alt']; ?>" class="overview-icon"/>
					<?php endif; ?>


					<h1><?php the_title(); ?></h1>
					<?php the_content(); ?>

					<a href="#callback" class="scroll"><button class="button-request-callback"><img src="<?php echo get_stylesheet_directory_uri(); ?>/library/images/general/phone-white-line.png" class="" alt="Phone" width="12">Request a callback</button></a>
				</header>

			</div>
		</div><?php /* row */ ?>




</div><?php /* header-wrap - Opened in header.php*/ ?>




			<!-- Sub Nav -->
			<section class="sub-nav">
				<div class="row">
					<div class="small-12 columns">

						<?php if( have_rows('panels') ): $i = 0; ?>
							
							<p><span class="title">How we can help</span><span class="arrow">&rarr;</span>

							<?php while( have_rows('panels') ): the_row(); $i++; ?>

								<?php 
									//Get the title
									$title = get_sub_field('title');
									//Swap spaces for hyphens
									$title_id = preg_replace('/\s+/', '-', $title);
									//Convert to lowercase
									$title_id = strtolower($title_id);
								?>

								<?php if(get_sub_field('title')) : ?>
									<?php if(get_sub_field('find_out_more_link')) : ?>
										<a href="#<?php echo ($title_id); ?>" class="scroll"><?php the_sub_field('title'); ?></a>
									<?php else : ?>
										<a href="#<?php echo ($title_id); ?>" class="scroll"><?php the_sub_field('title'); ?></a>
									<?php endif; ?>
								<?php endif; ?>

							<?php endwhile; ?>

							</p>

						<?php endif; ?>
					</div>

					<div class="tp_-_category-badge" data-tp-settings="domainId:8191446,size:120">
						<a href="https://uk.trustpilot.com/review/www.thesmartlist.co.uk" hidden>The Smart List Reviews</a>
					</div>
					<script type="text/javascript">
					  (function(){ var a = "https:" == document.location.protocol ? "https://s.trustpilot.com" : "http://s.trustpilot.com", b=document.createElement("script");b.type="text/javascript";b.async=true;b.src=a+"/tpelements/tp_elements_all.js";var c=document.getElementsByTagName("script")[0];c.parentNode.insertBefore(b,c)})();
					</script>

				</div>
			</section>


			<div class="row services-wrap page-styles">
				<div class="small-12 columns">


					<!-- Service Panels -->
					<?php if( have_rows('panels') ): $i = 0; ?>
						<?php while( have_rows('panels') ): the_row(); $i++; ?>
					 	
							<?php 
								$title = get_sub_field('title');
								$title_id = preg_replace('/\s+/', '-', $title);
								$title_id = strtolower($title_id);
							?>

							<?php /* $i variable increments with each post. Use $i value on the end of file path to change background iamge. */ ?>
							<section id="<?php echo $title_id; ?>" class="service-wrap" style="background-image: url('<?php echo get_stylesheet_directory_uri(); ?>/library/images/product-service-bg-<?php echo $i; ?>.png');">
								
								<?php if ($i == 3) {
									//Set $i variable back to zero so that fourth section uses 1st background image, 5th section uses 2nd and so on...
									$i = 0;
								} ?>

 								<?php if(get_sub_field('thumbnail')) : ?>
 									<div class="service-icon">
										<?php $image = wp_get_attachment_image_src(get_sub_field('thumbnail'), 'thumbnail'); ?>
										<img src="<?php echo $image[0]; ?>" alt="<?php echo get_the_title(get_sub_field('thumbnail')) ?>" />
									</div>
								<?php endif; ?>

								<div class="service-content">
									<header>
										<?php if(get_sub_field('title')) : ?><h2><?php the_sub_field('title'); ?></h2><?php endif; ?>
										<?php if(get_sub_field('subtitle')) : ?><h3><?php the_sub_field('subtitle'); ?></h3><?php endif; ?>
										<hr>
									</header>

									<?php if(get_sub_field('content')) : ?><span class="content"><?php the_sub_field('content'); ?></span><?php endif; ?>

									<a href="#callback" class="scroll"><button class="button-request-callback"><img src="<?php echo get_stylesheet_directory_uri(); ?>/library/images/general/phone-white-line.png" class="" alt="Phone" width="12">Request a callback</button></a>
								</div>

								<div class="clear"></div>
							</section>

						<?php endwhile; ?> 
					<?php endif; ?>


				</div>
			</div><?php /* row */ ?>



			<!-- Callback -->
			<section class="page-styles callback-section" id="callback">
				<div class="row">
					<div class="small-12 columns">

						<div class="standard-form-wrap">

							<div class="form-details">
								<h2 class="static-border">Request a Callback</h2>
								<p>We'll call you. Just give us a few details and let us know what you're interested in.</p>
								<p>We'll then arrange for an advisor to get in touch with you straight away* and fill you in on exactly what you need to know. Nice and easy.</p>
								<p>Alertnatively, if you'd like an immediate response, call us on: <?php the_field('tel_eng','options'); ?></p>
								<p><small>* We're able to call between 9am - 5pm</small></p>
							</div>

							<?php 
						    $form = get_field('form_to_display');
						    gravity_form_enqueue_scripts($form->id, true);
						    gravity_form($form->id, false, false, false, '', true, 1); 
							?>

							<div class="clear"></div>
						</div><?php /* form-wrap */ ?>

					</div>
				</div><?php /* row */ ?>
			</section>


		

	<?php endwhile; ?>
			
	<?php else : ?>	
	<?php endif; ?>
			
		




	<?php get_template_part( 'partials/content', 'trustpilot' ); ?> 



<?php get_footer();