<?php

/* Template Name: Upload CV  */

get_header(); ?>

	<?php if (is_page(39)){
		//About page conditional check
		$classes = 'about-page-wrap';
	}?>


	<!-- START: Breadcrumb -->
	<?php get_template_part( 'partials/content', 'breadcrumb' ); ?>
	<!-- END: Breadcrumb -->

	<div class="page-styles <?php if(isset($classes)){echo $classes;}?>">

		<div role="main">

			<?php if (have_posts()) : while (have_posts()) : the_post(); ?>

				<article id="post-<?php the_ID(); ?>" <?php post_class(); ?> role="article">

					<section class="entry-content" id="uploadCV">


						<div class="row">
							<div class="large-12 columns">

								<div class="entry-text">
									<h1><?php the_title(); ?></h1>

                  <?php the_content(); ?>
								</div>

		            <div class="form-wrap">

		            	<div class="upload-cv-form-title">
										<?php echo get_field('form_title'); ?>
									</div>

		            	<?php echo do_shortcode('[gravityform id="'.get_field('form_id').'" title="false" description="false"]'); ?>

		            	<div class="<?php echo $_SERVER['REQUEST_METHOD'] == 'POST' ? 'hide' : ''; ?> form-warning">
		            		<?php echo get_field('form_warning_text'); ?>
		            	</div>
		            </div>

							</div>

						</div>



            <div class="upload-cv-content-container">
	            <div class="row upload-cv-page-details">
		            <div class="large-6 col-left columns">

									<?php
										$left_part_title = get_field('left_part_title');
										$left_part_text = get_field('left_part_text');
									?>
									<?php if( $left_part_title <> '' ): ?>
									<h2><?php echo $left_part_title; ?></h2>
									<?php endif; ?>

									<?php if( $left_part_text <> '' ): ?>
									<?php echo $left_part_text; ?>
									<?php endif; ?>

								</div>

		            <div class="large-6 col-right columns">

									<?php
										$right_part_title = get_field('right_part_title');
										$right_part_text = get_field('right_part_text');
									?>
									<?php if( $right_part_title <> '' ): ?>
									<h2><?php echo $right_part_title; ?></h2>
									<?php endif; ?>

									<?php if( $right_part_text <> '' ): ?>
									<?php echo $right_part_text; ?>
									<?php endif; ?>

									<div class="video">
	              		<?php echo get_field('video_embed'); ?>
	      					</div>

								</div>

							</div>
						</div>

					</section>

				</article>

			<?php endwhile; ?>

			<?php else : ?>

				<article class="post-not-found">

					<header class="not-found-header">

						<h2><?php _e( 'Nothing Found!' ); ?></h2>

					</header>

					<section class="not-found-content">

						<p><?php _e( 'Please check what you are looking for' ); ?></p>

					</section>

				</article>

			<?php endif; ?>
		</div>

		<?php
		$facts = get_field('facts_box');
		$get_in_touch = get_field('get_in_touch_box');
		?>


		<?php if(!empty($facts) || !empty($get_in_touch)): ?>

			<aside class="small-12 medium-12 large-4 columns">

				<?php if(!empty($facts)): ?>
					<div class="aside-content">
						<h2 class="static-border">Facts</h2>
						<?php the_field('facts_box'); ?>
					</div>
				<?php endif; ?>

				<?php if(!empty($get_in_touch)): ?>
					<div class="aside-content get-in-touch">
						<h2 class="static-border">Get in touch</h2>
						<?php the_field('get_in_touch_box'); ?>
					</div>
				<?php endif; ?>


				<a href="<?php bloginfo( 'url' ); ?>/contact/">
					<button class="button-request-callback">
						<img src="<?php echo get_stylesheet_directory_uri(); ?>/library/images/general/phone-white-line.png" class="" alt="Phone" width="12">
						Request a callback
					</button>
				</a>

			</aside>

		<?php endif; ?>

	</div><?php /* row */ ?>




	<!-- START: Trustpilot -->
	<?php get_template_part( 'partials/content', 'trustpilot' ); ?>
	<!-- END: Trustpilot -->

<?php get_footer();