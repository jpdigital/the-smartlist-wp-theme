	
/*********************
All js stuff below
*********************/
	
jQuery( document ).ready(function($) {


  $(document).foundation();


	/**
		OWL carousel banner
	**/
  function countHomepageSliders() {
    $slideCount = $('.page-home #home-banner-carousel .slider-item').length;
    
    if ( $slideCount >= 2 ) { 
      $('#home-banner-carousel').owlCarousel({
        loop:true,
        items: 1,
        autoplay:true,
        autoplayTimeout:3000,
        autoplayHoverPause:true,
        nav: true,
        autoHeight: true,
        dots: true
      });
    }
     
  }
    
  countHomepageSliders();
  

  //Remove videos from homepage on mobile devices
  var ua = navigator.userAgent;
  var checker = {
    iphone: ua.match(/(iPhone|iPod|iPad)/),
    blackberry: ua.match(/BlackBerry/),
    android: ua.match(/Android/)
  };

  if (checker.iphone){
    $('.page-home .hero-video #heroVideo').remove();
    $('.page-home .customer-service-video #customerService').remove();
  }

  if (checker.android){
    $('.page-home .hero-video #heroVideo').remove();
    $('.page-home .customer-service-video #customerService').remove();
  }

  if (checker.blackberry){
    $('.page-home .hero-video #heroVideo').remove();
    $('.page-home .customer-service-video #customerService').remove();
  }

  /*********************
  GLOBAL
  *********************/
  
  //Mobile navigation
  $('.mobile-menu-trigger').click(function(){
  	$(this).toggleClass('active');
  	$('.recruiter-bar').toggleClass('push-left');
  	$('.menu-wrap').toggleClass('menu-wrap-visible');
    $('.page-header').toggleClass('mobile-menu-open');
    $('.sl-logo-notype').toggleClass('top-position-changed')
  });


  //Mobile dropdowns
  $('.mobile-dropdown-trigger').click(function(){
    $(this).toggleClass('dropdown-visible');
    $(this).siblings('.sub-menu').toggleClass('menu-dropdown-visible');
  });


  //Smooth Scroll
  $('a.scroll').click(function(){
    $('html, body').animate({
      scrollTop: $( $.attr(this, 'href') ).offset().top
    }, 500);
    return false;
  });


  



  /*ISSUE BEGINS HERE*/
  if($('select').length){
  $('select').selectBox({ hideOnWindowScroll: false });
  }
  resetChecks = function(elm){
     var _checkboxes = $('.checkbox-link').not(elm);
    _checkboxes.each(function(){
        $(this).removeClass('checked');
         _checkbox = $('input',$(this));
         _checkbox.prop('checked',false);
   })
  }

  $('.checkbox-link').on('click',function(e){
     e.preventDefault();
   resetChecks($(this));
          _checkbox = $('input',$(this));
   //reset
   
    if(!$(this).hasClass('checked')){

      $(this).addClass('checked');
      _checkbox.prop('checked',true);
    } else {
      $(this).removeClass('checked');
       _checkbox.prop('checked',false);
    }
  })
  /*ISSUE ENDS HERE*/







  /*********************
  HOME PAGE
  *********************/

  //Homepage hero video
  var hero_height = $('.hero-video').innerHeight();
  //The following makes use of the jquery waypoints plugin
  //When scrolling up the page, play the video as soon as it enters the viewport, pause it otherwise
  //The offset is set to the height of the video
  $('.hero-video').waypoint(function(direction) {
    if (direction === 'up') {
      $('#heroVideo').get(0).play();
    } else {
      $('#heroVideo').get(0).pause();
    }
  }, {
    offset: -hero_height
  });



  //If the URL contains '#video', then open the fancybox and autoplay the video
  //Get URL
  var url = window.location.href;
  //Remove http:// or https://
  var urlNoProtocol = url.replace(/^https?\:\/\//i, "");
  //Split tue URL by it's slash delimiters
  urlFindHash = urlNoProtocol.split("/");
  //If the first item in the array is #video, launch and play the modal
  if(urlFindHash[1] == '#video') {
    var videoLink = $('.play').data('src');
    $.fancybox( '<iframe width="800" height="600" src="'+videoLink+'&autoplay=1" frameborder="0" allowfullscreen style="max-width: 100%;"></iframe>' );
  }

  
  //Homepage hero video play
  $(document).on('click', '.play', function(e){
    e.preventDefault();
    var targetLink = $(this).data('src');
    console.log(targetLink);

    $.fancybox.open({
      href : targetLink, 
      closeClick  : false,
      openEffect  : 'fade',
      closeEffect : 'fade',
      padding : 0,
      helpers : {
        media : {}
      }
    });
  });
  


  //Homepage customer service video
  //The following makes use of the jquery waypoints plugin
  //When scrolling down the page, play the video as soon as it enters the viewport.
  $('.customer-service-video').waypoint(function(direction) {
    if (direction === 'down') {
      $('#customerService').get(0).play();
    } else {
      $('#customerService').get(0).pause();
    }
  }, {
    offset: '100%'
  });



  var customer_service_video = $('.customer-service-video');
  if (customer_service_video.length) {
    //When scrolling, if the user scrolls past the customer service video, it will pause
    $(window).on('scroll', function() {
      var y_scroll_pos = window.pageYOffset;
      var bottom = $('.customer-service-video').position().top+$('.customer-service-video').outerHeight(true);
      if(y_scroll_pos > bottom) {
        $('#customerService').get(0).pause();
      }
    });
  }


  //Get the height of the customer service video div without the top margin
  var cs_video_height = $('.customer-service-video').innerHeight();

  //The following makes use of the jquery waypoints plugin
  //When scrolling up the page, play the video as soon as it enters the viewport.
  //The offset is set to the height of the video
  $('.customer-service-video').waypoint(function(direction) {
    if (direction === 'up') {
      $('#customerService').get(0).play();
    } else {
      $('#customerService').get(0).pause();
    }
  }, {
    offset: -cs_video_height
  });







  /*********************
  INSIGHTS ARTICLE
  *********************/
  $('.add-comment').click(function(){
    $('html, body').animate({
      scrollTop: $("#respond").offset().top
    }, 300);
    return false;
  });



  /*********************
  FIXED PRICE RECRUITMENT
  *********************/
  $(document).on('click','.package-header-wrap', function() {
    var list = $(this).siblings('ul');
    list.stop(false, false).slideToggle('1000', 'swing');
  });


});


