<?php get_header(); ?>
	
<?php get_template_part( 'partials/content', 'breadcrumb' ); ?>

<div class="row">

	<div class="small-12 large-8 columns" role="main">

		<?php while (have_posts()) : the_post(); ?>
			<article class="post-wrap">

				<?php if (has_post_thumbnail()) { ?>
					<figure><?php the_post_thumbnail('full') ?></figure>
				<?php } else { ?>
					<figure><img src="<?php echo get_stylesheet_directory_uri(); ?>/library/images/general/insight-placeholder.png" class="" alt="Insights"></figure>
				<?php }?>

				<header>	
					<h2><?php the_title(); ?></h2>
					<time datetime="<?php the_time( 'Y-m-d' ); ?>" pubdate><?php the_time('F jS, Y'); ?></time>
				</header>

				<?php the_content(); ?>

			</article>


			<section class="social row">

				<div class="large-12 columns">
					<div class="inner">

						<div class="share">
							<a href="mailto:?subject=<?php bloginfo('name');?> - <?php the_title(); ?>&amp;body=<?php the_permalink(); ?>" title="Share by Email"><button>Email This Article</button></a>
							<nav><?php get_template_part( 'partials/content', 'sociallinks' ); ?></nav>
						</div><?php /* share */ ?>

						<div class="comments">
							<span><?php comments_number(); ?></span><a href="#"><button class="add-comment">Add Yours</button></a>
						</div><?php /* comments */ ?>

					</div><?php /* inner */ ?>
				</div>

			</section><?php /* row */ ?>


			<section class="page-style comments-wrap">
				<?php if ( comments_open() || get_comments_number() ) {
					comments_template(); 
				} ?>
			</section>


		<?php endwhile; ?>


	</div><?php /* small-12 */ ?>

	<div class="small-12 large-4 columns">
		<?php get_sidebar('single'); ?>
	</div>

</div><?php /* row */ ?>


<?php get_template_part( 'partials/content', 'trustpilot' ); ?>


<?php get_footer();