<div id="main-sidebar" class="sidebar" role="complementary">
	
	<?php get_recent(5,'Featured Jobs', 'job', 'job-loop-short'); ?>

	<?php if ( is_active_sidebar( 'Main Sidebar' ) ) : ?>
	
		<?php dynamic_sidebar( 'main-sidebar' ); ?>
	
	<?php endif; ?>

</div>