'use strict';
module.exports = function(grunt) {

	// Project configuration.
	grunt.initConfig({
		pkg: grunt.file.readJSON('package.json'),
	  sass: {
	    dist: {
	      options: {
	        style: 'compressed'
	      },
	      files: {
	      	'library/css/style.css': 'library/scss/style.scss'
	      }
	    }
	  },

		watch: {
			css: {
				files: '**/*.scss',
				tasks: ['sass']
			}
		}
	});
	// Load the Grunt plugins.
	grunt.loadNpmTasks('grunt-contrib-sass');
	grunt.loadNpmTasks('grunt-contrib-watch');
	
	// Register the default tasks.
	grunt.registerTask('default', ['sass:dist', 'watch']);

};