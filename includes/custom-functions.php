<?php
define('THEME_VERSION', '1.2');
/**
 * Remove related videos from YouTube
 **/
function filter_youtube( $content ) {
	
	preg_match_all('#\bhttps?://[^\s()<>]+(?:\([\w\d]+\)|([^[:punct:]\s]|/))#', $content, $match);
	
	$url = isset( $match[0][0] ) && $match[0][0] <> '' ? $match[0][0] : '';
	
	if( $url <> '' ) {
		$query = parse_url( $url, PHP_URL_QUERY);
    if ( $query ) {
      parse_str( $query, $queryParams );
      $queryParams['rel'] = '0';
      $new_url = str_replace("?$query", '?' . http_build_query($queryParams), $url);
    } 
    
    if( isset( $new_url ) ) {
    	$content = str_replace( $url, $new_url, $content );
    }
    
    
	}
	
	return $content;

}

function scripts_and_styles() {
   //only effect front-end of your website
	if (!is_admin() && $_SERVER['SCRIPT_NAME'] != '/wp-login.php') {


		// respond
		wp_register_script( 'respondjs', get_stylesheet_directory_uri() . '/library/js/libs/min/respond.min.js', array('jquery'), null, false );
		wp_enqueue_script( 'respondjs' );


		// modernizr (without media query polyfill)
		wp_register_script( 'modernizr', get_stylesheet_directory_uri() . '/library/js/libs/modernizr.custom.min.js', array(), '2.5.3', false );
		wp_enqueue_script( 'modernizr' );

		// TrustPilot script
		//wp_register_script( 'trustpilot', '//widget.trustpilot.com/bootstrap/v5/tp.widget.bootstrap.min.js', array(), '', false );
		//wp_enqueue_script( 'trustpilot' );

		// register main stylesheet
		wp_register_style( 'stylesheet', get_stylesheet_directory_uri() . '/library/css/style.css', array(), THEME_VERSION, 'all' );
		wp_enqueue_style( 'stylesheet' );


		//register styles for our theme
		wp_register_style( 'respgrid', get_template_directory_uri() . '/library/css/foundation.css', array(), 'all' );
		wp_enqueue_style( 'respgrid' );

		//register fancybox styles
		wp_register_style( 'fancybox-css', get_template_directory_uri() . '/library/css/libs/fancybox/jquery.fancybox.css', array(), 'all' );
		wp_enqueue_style( 'fancybox-css' );
		
		//OWL carousel CSS
		wp_register_style( 'owl-carousel', get_template_directory_uri() . '/library/css/libs/owl-carousel.css', array(), 'all' );
		wp_enqueue_style( 'owl-carousel' );

		//register selectbox
		wp_register_script( 'selectbox', get_stylesheet_directory_uri() . '/library/js/libs/jquery.selectBox.js', array(), null, true );
		wp_enqueue_script( 'selectbox' );

		//register waypoints
		wp_register_script( 'waypoints', get_stylesheet_directory_uri() . '/library/js/libs/jquery.waypoints.min.js', array('jquery'), null, true );
		wp_enqueue_script( 'waypoints' );

		//register fancybox scripts
		wp_register_script( 'fancybox-js', get_stylesheet_directory_uri() . '/library/js/libs/jquery.fancybox.js', array('jquery'), null, true );
		wp_enqueue_script( 'fancybox-js' );

		//register fancybox media helper script
		wp_register_script( 'fancybox-js-media', get_stylesheet_directory_uri() . '/library/js/libs/jquery.fancybox-media.js', array('jquery'), null, true );
		wp_enqueue_script( 'fancybox-js-media' );
		
		//register OWL carousel
		wp_register_script( 'owl-carousel', get_stylesheet_directory_uri() . '/library/js/libs/min/owl.carousel.min.js', array('jquery'), null, true );
		wp_enqueue_script( 'owl-carousel' );

		//register foundation + equalizer
		wp_register_script( 'foundation-js', get_stylesheet_directory_uri() . '/library/js/libs/min/foundation.min.js', array('jquery'), null, true );
		wp_enqueue_script( 'foundation-js' );

		//register all scripts
		wp_register_script( 'allscripts', get_stylesheet_directory_uri() . '/library/js/scripts.js', array(), null, true );
		wp_enqueue_script( 'allscripts' );


	}
}

// enqueue base scripts and styles
add_action('wp_enqueue_scripts', 'scripts_and_styles', 999);



// enqueue google fonts
function web_fonts() {
  wp_register_style('fira_font', '//code.cdn.mozilla.net/fonts/fira.css');

  wp_enqueue_style( 'fira_font');
}

add_action('wp_print_styles', 'web_fonts');




//Hide Admin Bar
//show_admin_bar( false );


/** Register Navigation Menus
 **/

function register_my_menus() {
	  register_nav_menus(
	    array( 	'main-menu'	   => __( 'Main Menu' ),
	            'footer-company' => __( 'Footer Menu - Company' ),
	            'footer-employers' => __( 'Footer Menu - Employers' ),
	            'footer-jobseekers' => __( 'Footer Menu - Jobseekers' ),
	   	)
	  );
	}

	add_action( 'init', 'register_my_menus' );


/** Custom taxonomies
 **/
register_taxonomy(
	'job_cat',
	'job',
	array( 
		'hierarchical' => false, 
		'label' => 'Job Category',
		'show_ui' => true,
		'query_var' => false,
		'rewrite' => array(
			'slug' => 'jobs/category', 
			'hierarchical' => false
		),
		'singular_label' => 'Job Category'
	) 
);

register_taxonomy(
	'job_type',
	'job',
	array( 
		'hierarchical' => false, 
		'label' => 'Contract Type',
		'show_ui' => true,
		'query_var' => false,
		'rewrite' => array(
			'slug' => 'jobs/type', 
			'hierarchical' => true
		),
		'singular_label' => 'Contract Type'
	) 
);

register_taxonomy(
	'job_time',
	'job',
	array( 
		'hierarchical' => false, 
		'label' => 'Contract Time',
		'show_ui' => true,
		'query_var' => false,
		'rewrite' => array(
			'slug' => 'jobs/contract-time', 
			'hierarchical' => true
		),
		'singular_label' => 'Contract Time'
	) 
);


/** Custom post types
 **/
register_post_type('job', 
	array(	
		'label' => 'Jobs',
		'description' => '',
		'public' => true,
		'show_ui' => true,
		'show_in_menu' => true,
		'capability_type' => 'post',
		'hierarchical' => true,
		'rewrite' => array('slug' => 'jobs'),
		'query_var' => true,
		'has_archive' => true,
		'menu_position' => 4,
		'supports' => array('title', 'editor', 'thumbnail','excerpt'),'labels' => array (
  'name' => 'Jobs',
  'singular_name' => 'Job',
  'menu_name' => 'Jobs',
  'add_new' => 'Add New',
  'add_new_item' => 'Add New Job',
  'edit' => 'Edit',
  'edit_item' => 'Edit Job',
  'new_item' => 'New Job',
  'view' => 'View',
  'view_item' => 'View Job',
  'search_items' => 'Search Jobs',
  'not_found' => 'No Jobs Found',
  'not_found_in_trash' => 'No Jobs found in Trash',
  'parent' => 'Parent Job',
),) );

register_post_type('js_post', array(	'label' => 'Jobseeker Articles','description' => '','public' => true,'show_ui' => true,'show_in_menu' => true,'capability_type' => 'post','hierarchical' => false,'rewrite' => array('slug' => 'jobseekers/articles'),'query_var' => true,'has_archive' => true,'menu_position' => 4,'supports' => array('title', 'editor', 'thumbnail','excerpt', 'comments'),'labels' => array (
  'name' => 'Jobseeker Articles',
  'singular_name' => 'Article',
  'menu_name' => 'Jobseeker Articles',
  'add_new' => 'Add New',
  'add_new_item' => 'Add New Article',
  'edit' => 'Edit',
  'edit_item' => 'Edit Article',
  'new_item' => 'New Article',
  'view' => 'View',
  'view_item' => 'View Article',
  'search_items' => 'Search Jobseeker Articles',
  'not_found' => 'No Jobseeker Articles Found',
  'not_found_in_trash' => 'No Jobseeker Articles found in Trash',
  'parent' => 'Parent Article',
),) );


/** Add columns to CPT admin
 **/

add_filter( 'manage_taxonomies_for_job_columns', 'job_columns' );
function job_columns( $taxonomies ) {
    $taxonomies[] = 'job_cat';
    $taxonomies[] = 'job_type';
    $taxonomies[] = 'job_time';
    return $taxonomies;
}


/** ACF options pages
 **/

if( function_exists('acf_add_options_page') ) {

	acf_add_options_page('Site Settings');

}

/** Get related posts by taxonomy
 **/

function get_related( $pid, $numposts = 4,$title = 'Related',$posttype = 'post',$template = NULL,  $taxonomy   ) {
// get the custom post type's taxonomy terms
	$custom_taxterms = wp_get_object_terms( $pid, $taxonomy, array('fields' => 'ids') );
	// arguments
	$args = array(
	'post_type' => $posttype,
	'post_status' => 'publish',
	'posts_per_page' => $numposts,
	'orderby' => 'rand',
	'tax_query' => array(
	    array(
	        'taxonomy' => $taxonomy,
	        'field' => 'id',
	        'terms' => $custom_taxterms
	    )
	),
	'post__not_in' => array ($pid),
	);
	$related_items = new WP_Query( $args );
	// loop over query
	if ($related_items->have_posts()) :
		echo '<h3>' . $title  . '</h3><ol>';
		while ( $related_items->have_posts() ) : $related_items->the_post();
	?>
	    <?php if($template) : ?>
			<?php get_template_part( 'partials/content', $template ); ?>
		<?php else : ?>
	    	<li><a href="<?php the_permalink(); ?>" title="<?php the_title_attribute(); ?>"><?php the_title(); ?></a></li>
		<?php endif; ?>
	<?php
		endwhile;
		echo '</ol>';
	else :
		echo '<h3>No related posts</h3>';
	endif;
	// Reset Post Data
	wp_reset_postdata();
}

/** Get recent posts by taxonomy & post_type with optional partial template
 **/

function get_recent($numposts = 4, $title = 'Recent', $posttype = 'post', $template = NULL, $taxonomy, $term_id , $showfallback = false, $fallbacktemplate = NULL ) {
	// arguments
	if($taxonomy != NULL) {
		$args = array(
			'post_type' => $posttype,
			'post_status' => 'publish',
			'posts_per_page' => $numposts,
			'orderby' => 'ASC',
			'tax_query' => array(
			    array(
			        'taxonomy' => $taxonomy,
			        'field' => 'id',
			        'terms' => $term_id
			    )
			),
		);

	} else {

	$args = array(
		'post_type' => $posttype,
		'post_status' => 'publish',
		'posts_per_page' => $numposts,
		'orderby' => 'ASC',
	);

	};

	$recent_items = new WP_Query( $args );
	// loop over query
	if ($recent_items->have_posts()) :
		echo '<h3>' . $title  . '</h3><ol class="row">';
		while ( $recent_items->have_posts() ) : $recent_items->the_post();
	?>
		<?php if($template) : ?>
			<?php
				get_template_part( 'partials/content', $template );
			?>
		<?php else : ?>
	    	<li><a href="<?php the_permalink(); ?>" title="<?php the_title_attribute(); ?>"><?php the_title(); ?></a></li>
		<?php endif; ?>
	<?php
		endwhile;

		if($showfallback === TRUE) :
			$wefound = $recent_items->found_posts;
			//echo $wefound;
			if($wefound < $numposts) :
				get_template_part( 'partials/content', $fallbacktemplate );
			endif;
		endif;

		echo '</ol>';
	else :
		echo '<h3>No recent posts</h3>';
	endif;
	// Reset Post Data
	wp_reset_postdata();
}

/** Get recent posts no taxonomy
 **/

function get_recent_notax($numposts = 4, $title = 'Recent', $posttype = 'post', $template = NULL, $showfallback = false, $fallbacktemplate = NULL ) {
	// arguments
	if($taxonomy != NULL) {
		$args = array(
			'post_type' => $posttype,
			'post_status' => 'publish',
			'posts_per_page' => $numposts,
			'orderby' => 'ASC',
		);

	} else {

	$args = array(
		'post_type' => $posttype,
		'post_status' => 'publish',
		'posts_per_page' => $numposts,
		'orderby' => 'ASC',
	);

	};

	$recent_items = new WP_Query( $args );
	// loop over query
	if ($recent_items->have_posts()) :
		echo '<h3>' . $title  . '</h3><ol class="row">';
		while ( $recent_items->have_posts() ) : $recent_items->the_post();
	?>
		<?php if($template) : ?>
			<?php
				get_template_part( 'partials/content', $template );
			?>
		<?php else : ?>
	    	<li><a href="<?php the_permalink(); ?>" title="<?php the_title_attribute(); ?>"><?php the_title(); ?></a></li>
		<?php endif; ?>
	<?php
		endwhile;

		if($showfallback === TRUE) :
			$wefound = $recent_items->found_posts;
			//echo $wefound;
			if($wefound < $numposts) :
				get_template_part( 'partials/content', $fallbacktemplate );
			endif;
		endif;

		echo '</ol>';
	else :
		echo '<h3>No recent posts</h3>';
	endif;
	// Reset Post Data
	wp_reset_postdata();
}

/** Get most commented posts (optional taxonomy, post_type, partial template settings)
 **/

function get_most_commented($numposts = 4, $title = 'Most popular', $posttype = 'post',  $template = NULL, $taxonomy = NULL, $term_id = NULL ) {

	// arguments
	if($taxonomy != NULL) {
		$args = array(
			'post_type' => $posttype,
			'post_status' => 'publish',
			'posts_per_page' => $numposts,
			'orderby' => 'comment_count',
			'tax_query' => array(
			    array(
			        'taxonomy' => $taxonomy,
			        'field' => 'id',
			        'terms' => $term_id
			    )
			),
		);

	} else {

	$args = array(
		'post_type' => $posttype,
		'post_status' => 'publish',
		'posts_per_page' => $numposts,
		'orderby' => 'comment_count',
	);

	};

	$most_commented = new WP_Query( $args );
	// loop over query
	if ($most_commented->have_posts()) :
		echo '<h3>' . $title  . '</h3><ol class="row">';
		while ( $most_commented->have_posts() ) : $most_commented->the_post();
	?>
		<?php if($template) : ?>
			<?php get_template_part( 'partials/content', $template ); ?>
		<?php else : ?>
	    	<li><a href="<?php the_permalink(); ?>" title="<?php the_title_attribute(); ?>"><?php the_title(); ?></a></li>
		<?php endif; ?>
	<?php
		endwhile;
		echo '</ol>';
	else :
		echo '<h3>No posts to display</h3>';
	endif;
	// Reset Post Data
	wp_reset_postdata();
}


/** Build a select form field from custom taxonommies
 **/

function build_select($tax, $firstoption){
	$terms = get_terms($tax);
	$current = '';
	if(isset($_GET['job_cat'])) {
		$current = $_GET['job_cat'];
	};
	$x = '<select name="'. $tax .'">';
	$x .= '<option value="">'. $firstoption .'</option>';
	foreach ($terms as $term) {
	   $x .= '<option value="' . $term->term_id . '"';
	   if(($current == $term->term_id) || (get_queried_object()->term_id == $term->term_id)) { $x .= 'selected="selected"'; };
	   $x .= '>' . $term->name . '</option>';
	}
	$x .= '</select>';
	echo $x;
}

/** Build checkboxes from custom taxonommies
 **/

function build_checkboxes($tax, $fieldname){
	$terms = get_terms($tax);
	$current = '';
	if(isset($_GET['job_type'])) {
		if(is_array($_GET['job_type'])){
			$current = implode(',',$_GET['job_type']);
		}
		else {	
			$current = $_GET['job_type'];
		}
	}
	
	$x = '';
	foreach ($terms as $term) {
		$matched = false;
		if((is_array($current) && in_array($term->term_id, $current)) || $term->term_id == $current){
			$matched = true;
		}
		if ($matched){
			$x .= '<label class="medium-6 large-3 columns"><div class="checkbox-link checked"><span class="vcenter"><input type="checkbox" name="' . UCFirst($fieldname) . '[]" value="' . $term->term_id . '"';		
		}
		else {
			$x .= '<label class="medium-6 large-3 columns"><div class="checkbox-link"><span class="vcenter"><input type="checkbox" name="' . UCFirst($fieldname) . '[]" value="' . $term->term_id . '"';
		}
	  if((is_array($current) && in_array($term->term_id, $current)) || $term->term_id == $current) { $x .= 'checked="checked"'; };
	  $x .= '><span class="checkbox"></span><span class="label">' . UCFirst($term->name) . '</span></span></div></label>';
	};
	echo $x;
}

/** Advanced search results
 **/

function advanced_search_results($wp_query) {

	if ( !is_admin() && $wp_query->is_main_query() ) {

    if ($wp_query->is_search) {

    	$search = sanitize_title_for_query($_GET['s']);
    	$location = sanitize_title_for_query($_GET['location']);
    	if(is_array($_GET['job_cat'])):
    		$job_cat = (string) implode(',',$_GET['job_cat']);
    	else:	
    		$job_cat = sanitize_title_for_query($_GET['job_cat']);
    	endif;
    	if(is_array($_GET['job_type'])):
    		$job_type = (string) implode(',',$_GET['job_type']);
    	else:	
    		$job_type = sanitize_title_for_query($_GET['job_type']);
    	endif;
    	$salary_min = sanitize_title_for_query($_GET['salary_min']);
    	$salary_max = sanitize_title_for_query($_GET['salary_max']);

    	// Set post type
			$wp_query->set('post_type', 'job' );
			//Onlybring back published items
			$wp_query->set('post_status', 'publish' );
			//Set it all up to order by job_priority so Smartlist feed jobs get priority.
			// $wp_query->set('meta_key', 'job_priority' );
			// $wp_query->set('orderby', 'meta_value_num' );
			// $wp_query->set('order', 'ASC' );

			// Add your criteria
	        $meta_query = array(
	                'key'     => 'job_priority',
	                'orderby'   => 'meta_value_num',
	                'order' => 'ASC',
	        );    
       		$wp_query->set('meta_query',$meta_query);

			$paged = ( get_query_var('paged') ) ? get_query_var('paged') : 1;
			$wp_query->set('paged', $paged );
		
			// Set tax queries
			if(strlen($job_type) > 0 && strlen($job_cat) > 0) {
				$wp_query->set( 'tax_query', array(
						'relation' => 'AND',
						array(
							'taxonomy' => 'job_type',
							'field'    => 'term_id',
							'terms'    => $job_type,
							'include_children' => false,
						),
						array(
							'taxonomy' => 'job_cat',
							'field'    => 'term_id',
							'terms'    => $job_cat,
							'include_children' => false,
						),
					)
				);
			} elseif(	strlen($job_type) > 0 ) {
				$wp_query->set( 'tax_query', array(
						array(
							'taxonomy' => 'job_type',
							'field'    => 'term_id',
							'terms'    => $job_type,
							'include_children' => false,
						),
					)
				);
			} elseif(	strlen($job_cat) > 0 ) {
				$wp_query->set( 'tax_query', array(
						array(
							'taxonomy' => 'job_cat',
							'field'    => 'term_id',
							'terms'    => $job_cat,
							'include_children' => false,
						),
					)
				);
			}

		//	We need to establish whether we need to search on salary & location as well.
		//	As these are meta query searches we have to work out what is in the meta query call
		
		$bLocSearch = false;
		$bSalarySearch = false;
		if( isset($location) && !empty($location) ) {
			$bLocSearch = true;
		}
		//	If either the minimum or maximum salary values have changed from defaults we include them
		if( $salary_min != 0 ) {
			//$bSalarySearch = true;
		}
		//	Now work out what type of meta query we need
		if($bLocSearch == true && $bSalarySearch == true)	{
			$searchType = 'both';
		}
		elseif($bLocSearch == true && $bSalarySearch == false){
			$searchType = 'location';
		}
		//Otherwise we're only search in salary range
		else{
			$searchType = 'none';
		}
		//	Now switch case over the 3 possibles.
		
		/*
		 * Change note 16/09/2019 by Jack Harper 
		 * To make the search compatible with imported data we need to disable salary search option and advert_active meta flag since no longer in use and causes 0 search results
		 */
		
		switch($searchType)	{
			case 'both': {
				$wp_query->set( 'meta_query', array(
						'relation' => 'AND',
						//Look in location, county and country and match on any
						array(
							'relation' => 'OR',
							array(
								'key'     => 'location',
								'value'   => $location,
								'compare' => 'LIKE',
							),
							array(
								'key'     => 'location_parent',
								'value'   => $location,
								'compare' => 'LIKE',
							),
							array(
								'key'     => 'country',
								'value'   => $location,
								'compare' => 'LIKE',
							),
						),
						//Now do a Between on Salary
						array(
							'key'     => 	'salary',
							'value'   => 	array($salary_min,$salary_max),
							'compare' => 	'BETWEEN',
							'type'		=>	'NUMERIC'
						),
						//Now only bring back Active jobs
						array(
							'key'     => 	'advert_active',
							'value'   => 	'1',
							'compare' => 	'=',
							'type'		=>	'NUMERIC'
						),
					)
				);

				break;
			}
			case 'location': {
				$wp_query->set( 'meta_query', 
						array(
							'relation' => 'OR',
							array(
								'key'     => 'location',
								'value'   => $location,
								'compare' => 'LIKE',
							),
							array(
								'key'     => 'location_parent',
								'value'   => $location,
								'compare' => 'LIKE',
							),
							array(
								'key'     => 'country',
								'value'   => $location,
								'compare' => 'LIKE',
							),
						)
				);

				break;
			}
			case 'salary': {
				$wp_query->set( 'meta_query', array(
						'relation' => 'AND',
						array(
							'key'     => 	'salary',
							'value'   => 	array($salary_min,$salary_max),
							'compare' => 	'BETWEEN',
							'type'		=>	'NUMERIC',
						),
						//Now only bring back Active jobs
						array(
							'key'     => 	'advert_active',
							'value'   => 	'1',
							'compare' => 	'=',
							'type'		=>	'NUMERIC'
						),
					)
				);


				break;
			}

		}


		}
		// echo "<pre>";
		// print_r ($wp_query);
		// echo "</pre>";
		
 	}

 return $wp_query;

}	

add_filter('pre_get_posts','advanced_search_results');




//Page Slug Body Class
function add_slug_body_class( $classes ) {
	global $post;
	if ( isset( $post ) ) {
		$classes[] = $post->post_type . '-' . $post->post_name;
	}
	return $classes;
}
add_filter( 'body_class', 'add_slug_body_class' );

//	Delete priority 2 jobs before running a new Adzuna feed
add_action('pmxi_before_xml_import', 'before_xml_import', 10, 1);

function before_xml_import($import_id) { // 2 is the ID of the Adzuna feed.
	// We only want to delete old jobs if we're re-runnign the Adzuna feed!
	if ($import_id == 2) {
		echo 'Deleting old Adzuna jobs';
		//	Now we need to get all the Post_IDs for Adzuna jobs currently in the system.
		global $wpdb;
		$postIDs = $wpdb->get_results("SELECT p.ID FROM wp_posts p, wp_postmeta wp1
			WHERE p.ID = wp1.post_id
			AND p.post_type = 'job'
			AND wp1.meta_key = 'job_priority'
			AND wp1.meta_value = 2");

		$arrDelete = array();
		foreach ($postIDs as $post):
			$arrDelete[] = $post->ID;
		endforeach;

		if(count($arrDelete) >= 1) {
			echo '<br>Posts to delete: '.count($arrDelete);
			//	Do a manual deletion as wp_delete_post takes way tooo long on 16K records...
			// Convert array to a string for an IN clause.
			$strDelete = implode(',', $arrDelete);
			//echo 'String of ids: '.$strDelete;
			echo '<br>Deleting post meta';
			//	Delete Post meta rows.
			$postMetaDelete = $wpdb->get_results("DELETE FROM wp_postmeta
				WHERE post_id IN ($strDelete)");

			$postDelete =  $wpdb->get_results("DELETE FROM wp_posts
				WHERE ID IN ($strDelete)");
		}
		else {
			echo 'No current jobs to delete';
		};
	}

}


//	Run through priority 1 jobs, set expiry dates and then expire anything that's past its sell by date!
/*add_action('pmxi_after_xml_import', 'after_xml_import', 10, 1);

function after_xml_import($import_id) { // 4 is the ID of the LogicMelon feed.
	if ($import_id == 4) {
		echo 'Only looking at LogicMelon jobs';
		//	Now we need to get all the Post_IDs for Adzuna jobs currently in the system.
		global $wpdb;


		$args = array(

			'post_type'			=>	'job',
			'post_status'		=>	'publish',
			'meta_query'		=>	array(
				array(
					'key'     => 'job_priority',
					'value'   => 1,
					'compare' => '=',
				),
			),
		);

		$priOneJobs = new WP_Query($args);

			
		if(count($priOneJobs->found_posts) > 0) {
			echo '<br>Have Jobs to update: '.$priOneJobs->found_posts;

			if($priOneJobs->have_posts()): 
				while($priOneJobs->have_posts()): 
					$priOneJobs->the_post();

					//var_dump($post);
					//	Grab meta for this job
					$postMeta = get_post_meta( $post->ID );
					//	We need these fields:
					$advertStart = new DateTime($postMeta['advert_start_date'][0]); // yyyymmdd
					$weeksToAdvertise = $postMeta['advertise_weeks'][0];
					$daysToAdvertise = $weeksToAdvertise*7;
					$advertEnd = $postMeta['advert_end_date'][0];
					$advertActive = $postMeta['advert_active'][0];
					$today = new DateTime('now');

					$dateInt = 'P'.$daysToAdvertise.'D';
					//	Initialise our new End to be the same date as the advert start - as we'll add the ad period onto it.
					$advertNewEnd = new DateTime($advertStart->format('Ymd'));

					//Add the nuber of days to advertise onto the advert start date
					$advertNewEnd->add(new DateInterval($dateInt));

					//	Now update the end date
					$advertEnd = $advertNewEnd;
					
					//	If the end date is before today then we need to expire the job.
					if($advertEnd < $today):
						update_post_meta($post->ID, 'advert_active', 0);
					endif;
				endwhile;
			endif;
		}
		else {
			echo 'No LogicMelon jobs to update';	
		};
	}

}*/


//	See this for reference of why!
//	http://wordpress.stackexchange.com/questions/160203/problem-loading-custom-post-type-admin-page
function prevent_job_meta_caching( WP_Query $wp_query ) {
    if ( 'job' == $wp_query->get( 'post_type' ) ) {
        $wp_query->set( 'update_post_meta_cache', false );
    }
}
if ( is_admin() ) {
    add_action( 'pre_get_posts', 'prevent_job_meta_caching' );
}



//Prevent Gravity Forms Ajax from scrolling to the top of the page
add_filter('gform_confirmation_anchor', 'gform_no_scroll');

function gform_no_scroll(){
	return false;
} 

/**
 * Upload a CV into IDIBU service after form submitting
 * where 10 in a hook name is form_id
 * */
add_action( 'gform_after_submission_10', 'post_cv_to_idibu', 10, 2);

function post_cv_to_idibu( $entry, $form ) {
	
	$name = rgar( $entry, '1' );
	$email = rgar( $entry, '2' );
	$phone = rgar( $entry, '3' );
	$free_text = rgar( $entry, '5' );
	$cv = rgar( $entry, '4' );
	$cv_name = basename( $cv );
	
	$url = 'http://ws.idibu.com/ws/rest/v1/applicants/add-cv?hash=e94cea8494a1dc1bcaa80abae1c0ecc2';
	$xml = '<?xml version="1.0" encoding="UTF-8"?><idibu>
	  <job>
	    <id>128888</id>
	    <portal>bvh</portal>
	  </job>
	  <cv>
	    <name>' . $cv_name . '</name>
	    <contents>' . base64_encode( file_get_contents( $cv ) ) . '</contents>
	  </cv>
	  <email>
	    <from>' . $email . '</from>
	    <subject>CV Application from website</subject>
	    <body>' . $free_text . '</body>
	  </email>
	</idibu>
	';


	$params = array('http' => array(
		'method' => 'POST',
		'content' => 'data='.urlencode( $xml )
	));
	
	$ctx = stream_context_create($params);
	$fp = fopen($url, 'rb', false, $ctx);
	
	
	
	if (!$fp) {
		throw new Exception("Problem with $url, $php_errormsg");
	}
	
	$response = @stream_get_contents($fp);
	
	if ($response === false) {
	throw new Exception("Problem reading data from $url, $php_errormsg");
	}
	
	$response = simplexml_load_string($response);

	
}