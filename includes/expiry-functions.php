<?php

function add_expiry_date_to_jobs( $post_id, $post = NULL, $update = NULL ) {

	if($post === NULL) {
		$post = get_post($post_id);
	}

	// If this is a revision, don't do anything
	if ( wp_is_post_revision( $post_id ) ) {
		return;
	}
	if ( $post->post_type !== 'job' ) {
		return;
	}

	$post_expiry = get_post_meta( $post_id, 'job_expiry_date', true );

	if(!$post_expiry || $post_expiry === '') {
		// Triple check that we're not applying this to any other post type
		if($post->post_type === 'job') {
			$expiry_date = add_days_to_date($post->post_date, 30);
			update_post_meta( $post_id, 'job_expiry_date', $expiry_date );
		} else {
			return;
		}

	}

}
add_action( 'wp_insert_post', 'add_expiry_date_to_jobs', 10, 3 );

function add_days_to_date($date,$days){

    $date = strtotime("+".$days." days", strtotime($date));
    return  date("Y-m-d", $date);

}

function find_expired_posts($date, $limit = false){
	global $wpdb;

	if(!$limit) {
		$sql = "SELECT post_id FROM `wp_postmeta` WHERE `meta_key` LIKE 'job_expiry_date' AND `meta_value` <= '" . $date . "' ORDER BY `meta_value` DESC";
	} else {
		$sql = "SELECT post_id FROM `wp_postmeta` WHERE `meta_key` LIKE 'job_expiry_date' AND `meta_value` <= '" . $date . "' ORDER BY `meta_value` DESC LIMIT " . $limit;
	}

	$expired_post_ids = $wpdb->get_col( $sql );

	if(is_array($expired_post_ids)) {
		return $expired_post_ids;
	} else {
		return false;
	}

}

function delete_expired_posts($expired_posts) {
	if(is_array($expired_posts)) {
		foreach($expired_posts as $expired_post) {
			// Remove the chosen post, the 2nd parameter 'true' bypasses the trash and forces deletion
			wp_delete_post($expired_post, true);
		}
		return true;
	} else {
		return false;
	}
}

function find_existing_posts_with_missing_expiry() {
	global $wpdb;

	$sql = "SELECT ID FROM wp_posts as posts WHERE posts.post_type = 'job' AND NOT EXISTS (
            	SELECT * FROM `wp_postmeta`
               	WHERE `wp_postmeta`.`meta_key` = 'job_expiry_date'
                AND `wp_postmeta`.`post_id`=posts.ID
            ) LIMIT 500";

	$posts_with_missing_expiry = $wpdb->get_col( $sql );

	if(is_array($posts_with_missing_expiry)) {
		return $posts_with_missing_expiry;
	} else {
		return false;
	}

}