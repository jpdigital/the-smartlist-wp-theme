<?php
/** Advanced search results
 **/
function advanced_search_results($query) {

if ( ! is_admin() ) {

    if ($query->is_search):

    	$search = $_GET['s'];
    	$location = $_GET['location'];
    	$job_cat = $_GET['job_cat'];
    	if (isset($_GET['job_type'])) { $job_type = $_GET['job_type'];};
    	if (isset($_GET['salary_min'])) { $salary_min = $_GET['salary_min'];};
    	if (isset($_GET['salary_max'])) { $salary_max = $_GET['salary_max'];};

        $post_type = $query->get('post_type');
        $post_type[] = 'job';
		$query->set('post_type', $post_type );

		if(($job_cat != '') && (isset($job_type))) {
			$current_query = 'job type and job cat';
			$tax_query = $query->get('tax_query');
	        $tax_query['relation'] = 'OR';
	        $tax_query[] = array(
				'relation' => 'AND',
		         array(
					'taxonomy' => 'job_cat',
					'field'    => 'term_id',
					'terms'    => array($_GET['job_cat'])
				),
				array(
	                'taxonomy' => 'job_type',
			        'field'    => 'term_id',
			        'terms'    => array($_GET['job_type']),
	            ),

			);
			$query->set( 'tax_query', $tax_query );
		}
		elseif(($job_cat != '')) {
			$current_query = 'job cat only';
			$tax_query = $query->get('tax_query');
	        $tax_query['relation'] = 'AND';
	        $tax_query[] = array(
				array(
	                'taxonomy' => 'job_cat',
			        'field'    => 'term_id',
			        'terms'    => array($_GET['job_cat']),
	            ),

			);
			$query->set( 'tax_query', $tax_query );
    	}
    	elseif(isset($job_type)) {
			$current_query = 'job type only';
			$tax_query = $query->get('tax_query');
	        $tax_query['relation'] = 'AND';
	        $tax_query[] = array(
				array(
	                'taxonomy' => 'job_type',
			        'field'    => 'term_id',
			        'terms'    => $_GET['job_type'],
	            ),
			);
			$query->set( 'tax_query', $tax_query );
    	};

		if(isset($_GET['location'])) {
			$meta_query = $query->get('meta_query');
	        //$meta_query['relation'] = 'OR';
	        $meta_query[] = array(
	                'key' => 'location',
	                'value' =>  array($_GET['location']),
	                'compare' => 'IN'
	        );
			$query->set( 'meta_query', $meta_query );
		};


		$meta_query = $query->get('meta_query');
        $meta_query['relation'] = 'AND';
        $meta_query[] = array(
                'key' => 'salary',
                'value' =>  array($_GET['salary_min'], $_GET['salary_max']),
                'compare' => 'BETWEEN'
        );
		$query->set( 'meta_query', $meta_query );

		echo ('<p>Current query:' . $current_query . '</p>');
		echo ('<p>Job cat: ' . $job_cat . '</p>');
		echo ('<p>Job type: </p>');
		var_dump ($job_type);

    endif;
 };
 return $query;

};

function advanced_search_results2($query) {

if ( ! is_admin() ) {

    if ($query->is_search):

    	$search = $_GET['s'];
    	$location = $_GET['location'];
    	$job_cat = $_GET['job_cat'];
    	$job_type = $_GET['job_type'];
    	$salary_min = $_GET['salary_min'];
    	$salary_max = $_GET['salary_max'];

    	$post_type = $query->get('post_type');
        $post_type[] = 'job';
		$query->set('post_type', $post_type );

		if(isset($job_type) && !empty($job_type)) {
			//$current_query = 'job type and job cat';
			//$tax_query = $query->get('tax_query');
	        //$tax_query['relation'] = 'OR';
	        $jobtype_query[] = array(
				array(
	                'taxonomy' => 'job_type',
			        'field'    => 'term_id',
			        'terms'    => array($_GET['job_type']),
	            ),

			);
			//$query->set( 'tax_query', $tax_query );
		};
		if(isset($job_cat) && !empty($job_cat)) {
			//$current_query = 'job type and job cat';
			//$tax_query = $query->get('tax_query');
	        //$tax_query['relation'] = 'OR';
	        $jobcat_query[] = array(
				array(
	                'taxonomy' => 'job_cat',
			        'field'    => 'term_id',
			        'terms'    => array($_GET['job_cat']),
	            ),

			);
			//$query->set( 'tax_query', $tax_query );
		};

		$tax_query = array_merge( $jobtype_query, $jobcat_query, array( 'relation' => 'AND' ) );
		$query->set( 'tax_query', $tax_query );


    endif;
 };

 return $query;

}

add_filter('pre_get_posts','advanced_search_results2');