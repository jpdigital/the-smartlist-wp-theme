<div id="main-sidebar" class="sidebar" role="complementary">

	<?php if(is_singular( 'js_post' )) : ?>
		<?php get_recent_notax(4,'Latest Articles', 'js_post', 'insights-short'); ?>
	<?php elseif(is_singular( 'post' )) : ?>
		<?php get_recent(4,'Latest Insights', 'post', 'insights-short','category',2); ?>
	<?php endif; ?>


	<div class="newsletter-signup">
		<h4 class="static-border">Recruitment Newsletter</h4>
		<p><small>Subscribe to stay up to date with the latest recruitment news and insights from The SmartList.</small></p>
		<?php gravity_form(8, false, false, false, '', true, 20); ?>
	</div>
	

	<?php if(is_singular( 'js_post' )) : ?>
		<?php get_most_commented(4,'Most popular','js_post','insights-short'); ?>
	<?php elseif(is_singular( 'post' )) : ?>
		<?php get_most_commented(4,'Most popular','post','insights-short'); ?>
	<?php endif; ?>

	<?php if ( is_active_sidebar( 'Main Sidebar' ) ) : ?>	
		<?php dynamic_sidebar( 'main-sidebar' ); ?>
	<?php endif; ?>

</div>