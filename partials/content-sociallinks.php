<ul>
  <li><a href="https://www.facebook.com/sharer/sharer.php?u=<?php the_permalink(); ?>" target="_blank"><span class="icon icon-facebook"></span></a></li>
  <li><a href="https://twitter.com/home?status=<?php the_permalink(); ?>" target="_blank"><span class="icon icon-twitter"></span></a></li>
  <li><a href="https://www.linkedin.com/shareArticle?mini=true&url=<?php the_permalink(); ?>&title=<?php the_title(); ?>&summary=&source=" target="_blank"><span class="icon icon-iconmonstr-linkedin-4-icon"></a></li>
  <li><a href="https://plus.google.com/share?url=<?php the_permalink(); ?>" target="_blank"><span class="icon icon-googleplus"></span></a></li>
</ul>