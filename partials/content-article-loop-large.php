<section class="latest-article">

	<article id="post-<?php the_ID(); ?>" role="article">

		<a href="<?php the_permalink() ?>" rel="bookmark" title="<?php the_title_attribute(); ?>">
			
			<div class="large-6 columns">
				<?php if (has_post_thumbnail()) { ?>
					<?php the_post_thumbnail('full') ?>
				<?php } else { ?>
					<figure><img src="<?php echo get_stylesheet_directory_uri(); ?>/library/images/general/insight-placeholder.png" class="" alt="Insights"></figure>
				<?php }?>
			</div>

		</a>

			<div class="large-6 columns">		
				<div class="story">

					<header class="article-header">
						<a href="<?php the_permalink() ?>" rel="bookmark" title="<?php the_title_attribute(); ?>"><h1 class="animated-border blue"><?php the_title(); ?></h1></a>
						<time datetime="<?php the_time( 'Y-m-d' ); ?>" pubdate><?php the_time('d/m/Y'); ?></time>
					</header>

					<section class="entry-content">
						<?php the_excerpt(); ?>
					</section>

				</div><?php /* story */ ?>

			</div>

		

	</article>
	
</section>