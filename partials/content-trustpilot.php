<div class="trustpilot-wrap">

<?php if(get_field('truspilot_embed_code', 'options')) : ?>
	<section class="row">
		<?php the_field('truspilot_embed_code', 'options'); ?>
	</section>
<?php endif; ?> 

</div>