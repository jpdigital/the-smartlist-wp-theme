

<!-- START: Featured Jobs -->
<?php if( have_rows('add_a_video') ): ?>
			<section class="page-styles section featured-videos">
				<div class="row">
					
			<?php
				$videos_title = get_field('videos_title');
				if( $videos_title <> '' ):
			?>

				<br /><h2><?php echo $videos_title; ?></h2>

			<?php endif; ?>
					
            <?php
               while ( have_rows('add_a_video') ) : the_row();

										echo '<div class="large-4 columns">';

                    echo filter_youtube( get_sub_field('embed_video') );
                    
										$video_description = get_sub_field('video_description');
										
										if( $video_description <> '' ) {
											echo '<p class="video-description">' . $video_description . '</p>';
										}

										$call_to_action_text = get_sub_field('call_to_action_label');
										if( $call_to_action_text <> '') {
											
											echo '<a href="' . get_sub_field('call_to_action_url') . '" class="button-request-callback cta-link">' . $call_to_action_text . '</a>';
											
										}
										
										echo '</div>';

                endwhile;

            ?>

				</div><?php /* row */ ?>
			</section>
			<!-- END: Featured Jobs -->
<?php
else :


endif;

?>
	