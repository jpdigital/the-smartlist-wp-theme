<article id="post-<?php the_ID(); ?>" <?php post_class(''); ?> role="article">

	<header class="article-header">

		<h4 class=""><a href="<?php the_permalink() ?>" rel="bookmark" title="<?php the_title_attribute(); ?>"><?php the_title(); ?></a></h4>

	</header>

	<section class="entry-content">
			<?php if(get_field('location')) : ?><p>Location: <?php the_field('location'); ?></p><?php endif; ?>
			<?php if(get_field('salary')) : ?><p>Salary: <?php the_field('salary'); ?></p><?php endif; ?>
			<p><?php the_terms( $post->ID, 'job_type', 'Contract type: ', ', ' ); ?></p>

	</section>

	<footer class="article-footer">


	</footer>

</article>