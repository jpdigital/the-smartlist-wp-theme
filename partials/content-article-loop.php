<?php
if( $c == 3) {
	$style = 'third';
	$c = 0;
}
else $style='';
?>

<article id="post-<?php the_ID(); ?>" class="small-12 large-4 columns <?php echo $style; ?>" role="article">
	<a href="<?php the_permalink() ?>">
		
		<?php if (has_post_thumbnail()) { ?>
			<figure><?php the_post_thumbnail('full') ?></figure>
		<?php } else { ?>
			<figure><img src="<?php echo get_stylesheet_directory_uri(); ?>/library/images/general/insight-placeholder.png" class="" alt="Insights"></figure>
		<?php }?>
		
	</a>

		<header class="article-header">
			<a href="<?php the_permalink() ?>">
				<h1 class="animated-border blue"><?php the_title(); ?></h1>
			</a>
			<time datetime="<?php the_time( 'Y-m-d' ); ?>" pubdate><?php the_time('d/m/Y'); ?></time>
		</header>

		<section class="entry-content">
			<?php the_excerpt(); ?>
		</section>
		
</article>