<li>
	<a href="<?php the_permalink(); ?>" title="<?php the_title_attribute(); ?>">
		<p><strong><?php the_title(); ?></strong></p>
		<p><?php the_terms( $post->ID, 'job_type', 'Contract type: ', ', ' ); ?></p>
		<?php if(get_field('location')) : ?><p>Location: <?php the_field('location'); ?></p><?php endif; ?> 
		<?php if(get_field('salary')) : ?><p>Salary: <?php the_field('salary'); ?></p><?php endif; ?> 
	</a>
</li>