<article id="post-<?php the_ID(); ?>" <?php post_class(''); ?> role="article">

	<header class="article-header bg">
		<h3><?php the_title(); ?></h3>
		<a href="<?php the_permalink() ?>" rel="bookmark" title="<?php the_title_attribute(); ?>"><button class="caps">More information</button></a>
	</header>

	<section class="entry-content row large-collapse">
		<div class="large-8 columns excerpt">
			<?php

			 	//wp_strip_all_tags(html_entity_decode(the_excerpt( '<span class="read-more">' . __( 'Read more &raquo;' ) . '</span>' ))); 
				//Remove all HTML-ness fromt he excerpt
				$excerpt = wp_strip_all_tags(html_entity_decode(get_the_excerpt()));
				//	Rebuild the read more which has now had its permalink removed.
				$readMore = '... <span class="read-more"><a href="'.get_permalink().'">Read more</a></span>';
				$excerpt = str_replace(' Read More', $readMore, $excerpt);
				// Output it
				echo '<p>'.$excerpt.'</p>';
			?>

		</div>

		<aside class="large-3 large-offset-1 columns">
			<?php if(get_field('location')) : ?><p><strong>Location:</strong> <?php the_field('location'); ?></p><?php endif; ?>
			<?php if(get_field('salary')) : ?><p><strong>Salary:</strong> <?php the_field('salary'); ?></p><?php endif; ?>
			<p><?php the_terms( $post->ID, 'job_type', 'Contract type: ', ', ' ); ?></p>
		</aside>
	</section>

</article>