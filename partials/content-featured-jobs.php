<?php 

	//	get 5 featured jobs - as this will be the content of a relationship field we need 
	$featured_jobs = get_field('featured_jobs');
	if($featured_jobs){
	
		$args = array(
			'post_type'  			=> 'job',
			'meta_key'				=> 'job_priority',
			'orderby'					=> 'meta_value_num',
			'order'						=> 'ASC',
			'posts_per_page' 	=> 5,
			'post__in' 				=> array($featured_jobs),
			'meta_query' 			=> array(
				'key'						=> 'job_priority',
				'value'					=> 1,
				'compare'				=> '='
			)
		); 

	}
	$featured = new WP_Query($args); 


	//	Otherwise fall back to latest 5 jobs by priority
	if($featured->found_posts == 0) {
		//	to check for its existance and then content before falling back to latest 5 jobs.
		$args = array(
			'post_type'  	=> 'job',
			'meta_key'		=> 'job_priority',
			'orderby'			=> 'meta_value_num',
			'order'				=> 'ASC',
			'posts_per_page' => 5,
		); 
	}

	$featured = new WP_Query($args); 

?>

<!-- START: Featured Jobs -->
			<section class="page-styles section featured-jobs">
				<div class="row">
					<div class="large-12 columns">
							<?php if ($featured->have_posts()) : ?>
								<h2 class="static-border static-border-large">Featured Jobs</h2>
								<?php while ($featured->have_posts()) : $featured->the_post(); ?>
									<?php get_template_part( 'partials/content', 'job-loop' ); ?>
								<?php endwhile; ?>

							<?php else : ?>
								<h2>No featured jobs to display</h2>
							<?php endif; ?>
						<?php wp_reset_query(); ?>
					</div>
				</div><?php /* row */ ?>
			</section>
			<!-- END: Featured Jobs -->


	