<article id="post-<?php the_ID(); ?>" <?php post_class('medium-6 large-12 columns end'); ?> role="article">

	<div class="row">
		<div class="small-4 medium-12 large-4 columns">
			<?php if (has_post_thumbnail()) { ?>
				<figure><a href="<?php the_permalink(); ?>" title="<?php the_title_attribute(); ?>"><?php the_post_thumbnail('small') ?></a></figure>
			<?php } else { ?>
				<figure><a href="<?php the_permalink(); ?>" title="<?php the_title_attribute(); ?>"><img src="<?php echo get_stylesheet_directory_uri(); ?>/library/images/general/insight-placeholder.png" class="" alt="Insights"></a></figure>
			<?php }?>
		</div>

		<div class="small-8 medium-12 large-8 columns">
			<header class="article-header">
				<h4><a href="<?php the_permalink() ?>" rel="bookmark" title="<?php the_title_attribute(); ?>"><?php the_title(); ?></a></h4>
			</header>
		</div>

	</div><?php /* row */ ?>

</article>