<article id="post-<?php the_ID(); ?>" class="large-4 column fallback" role="article">
			<div class="inner">	
				<div>
	<header class="article-header">
		
		<h1>Check back soon...</h1>
	
	</header>
	
	<section class="entry-content">
		<p>We'll be periodically publishing some fantastic resources available for you to download.</p>
		<p>We're just making sure they're all perfect, so come back soon and see what we've got!</p>
		<p><small>We'll see you soon!</small></p>
	</section>
</div>
	</div>
</article>