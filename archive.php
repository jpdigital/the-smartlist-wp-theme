<?php
get_header(); ?>

<!-- START: Breadcrumb -->
<?php get_template_part( 'partials/content', 'breadcrumb' ); ?>
<!-- END: Breadcrumb -->


<section class="articles page-styles">

	<div class="row">
		<div class="small-12 medium-12 columns" role="main">

			<?php
			$args = array(
				'category__not_in' => 9
			);
			$latest_insights = new WP_Query( $args ); ?>
			
			<?php $i = 0; if ($latest_insights->have_posts()) : $c = 1; while ($latest_insights->have_posts()) : $latest_insights->the_post(); $i++; $c++;?>
				
				<?php if($i == 1) : ?>
				<div class="row">
					<?php get_template_part( 'partials/content', 'article-loop-large' ); ?>
				</div>

				<?php else : ?>
					<?php if($i==2): ?>
						<section class="row">
					<?php endif ?>

					<?php include(locate_template('partials/content-article-loop.php')); ?>

				<?php endif; ?>
			
			<?php endwhile; ?>

				</section><?php /* row */ ?>
			
			<?php endif; ?>
			<?php wp_reset_query(); ?>
			
		</div>
	</div><?php /* row */ ?>

</section><?php /* articles */ ?>

<?php get_template_part( 'partials/content', 'trustpilot' ); ?> 

<?php get_footer();