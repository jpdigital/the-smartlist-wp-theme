<aside id="main-sidebar" class="sidebar" role="complementary">
	<?php /* Removed as not ready for go live. ?>
	<section class="profile">
		<?php if(get_field('profile_url','options')) : ?>
			<p><a href="<?php the_field('profile_url','options'); ?>"><button>Create/Manage Profile</button></a></p>
		<?php endif;  ?>
		*/ ?>

		<?php if(get_field('url')) : ?><p><a href="<?php the_field('url'); ?>" class="apply-btn">Apply for this position</a></p><?php endif; ?> 
	</section>

	<section class="social">
		<nav>
			<a href="mailto:?subject=<?php bloginfo('name');?> - <?php the_title(); ?>&amp;body=<?php the_permalink(); ?>" title="Share by Email"><button class="caps">Email this job</button></a>
			<?php get_template_part( 'partials/content', 'sociallinks' ); ?>
		</nav>
	</section>
	
	<section class="related">
	<?php get_related($post->ID, 3, 'Related Jobs','job', 'related-jobs', 'job_cat'); ?>
	</section>

</aside>