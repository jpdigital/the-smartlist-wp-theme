	<!--	</div>
		</div>
-->
	<?php // close 'outer-wrap' ?>


	<footer class="page-footer" role="contentinfo">

		<header class="row contact">
			<div class="small-12 medium-3 large-8 columns logo">
				<img src="<?php echo get_stylesheet_directory_uri(); ?>/library/images/general/sl-logo-white-2.png" class="" alt="The SmartList">
			</div>

			<div class="small-12 medium-9 large-4 columns">
				<ul>
					<?php if(get_field('tel_eng','options')) : ?><li><p>Call us today on <strong><a href="tel:<?php the_field('tel_eng','options'); ?>"><?php the_field('tel_eng','options'); ?></a></strong> <br class="break"/> or <strong><a href="<?php echo site_url('contact'); ?>">Request a callback</a></strong></p></li><?php endif; ?>
				</ul>
			</div>
		</header>


		<?php if(is_page_template('page-industry-specific.php') || is_page_template('page-industry-specific-alt.php')) : ?>

		<?php else : ?>

			<div class="row">
				<div class="small-12 large-6 columns">
					<div class="links">
						<h6 class="caps animated-border">Company</h6>
						<?php wp_nav_menu( array( 'theme_location' => 'footer-company', 'container' => 'nav','menu_id'=> 'footer-menu-company') ); ?>
					</div>

					<div class="links">
						<h6 class="caps animated-border">Employers</h6>
						<?php wp_nav_menu( array( 'theme_location' => 'footer-employers', 'container' => 'nav','menu_id'=> 'footer-menu-employers') ); ?>
					</div>

					<div class="links">
						<h6 class="caps animated-border">Jobseekers</h6>
						<?php wp_nav_menu( array( 'theme_location' => 'footer-jobseekers', 'container' => 'nav','menu_id'=> 'footer-menu-jobseekers') ); ?>
					</div>


					<div class="clear footer-social">
						<h6 class="caps animated-border">Join us on</h6>




						<ul>
						  <?php if(get_field('twitter', 'options')) : ?>
						  	<li><a href="<?php the_field('twitter', 'options'); ?>" target="_blank"><span class="icon-twitter"></span></a></li>
						  <?php endif; ?>

							<?php if(get_field('facebook', 'options')) : ?>
							  <li><a href="<?php the_field('facebook', 'options'); ?>" target="_blank"><span class="icon-facebook"></span></a></li>
						  <?php endif; ?>

							<?php if(get_field('youtube', 'options')) : ?>
						  	<li><a href="<?php the_field('youtube', 'options'); ?>" target="_blank"><span class="icon-iconmonstr-youtube-4-icon"></span></a></li>
						  <?php endif; ?>

						  <?php if(get_field('linkedin', 'options')) : ?>
						  	<li><a href="<?php the_field('linkedin', 'options'); ?>" target="_blank"><span class="icon-iconmonstr-linkedin-4-icon"></span></a></li>
						  <?php endif; ?>

						  <?php if(get_field('google_plus', 'options')) : ?>
						  	<li><a href="<?php the_field('google_plus', 'options'); ?>" target="_blank"><span class="icon-googleplus"></span></a></li>
						  <?php endif; ?>

						  <?php if(get_field('ning', 'options')) : ?>
						  	<li><a href="<?php the_field('ning', 'options'); ?>" target="_blank"><span class="icon-icon-ning"></span></a></li>
						  <?php endif; ?>
						</ul>
						<div class="clear"></div>
					</div>
				</div>

				<div class="small-12 large-4 large-offset-2 columns subscribe" id="subscribe">
					<h6 class="caps animated-border">Subscribe to our Recruiter Newsletter</h6>
					<p><small>Subscribe to stay up to date with the latest recruitment news and insights from The SmartList.</small></p>
					<?php gravity_form(4, false, false, false, '', true, 50); ?>
				</div>
			</div><?php /*row*/ ?>


			<footer class="row">
				<div class="small-12 columns center copyright">
					<p><small>Copyright &copy; <?php echo date('Y'); ?> JPIMedia Publishing Ltd.</small></p>
				</div>
			</footer><?php /*row*/ ?>

		<?php endif; ?>



	</footer>

<?php wp_footer(); ?>

<script src="//contentz.mkt922.com/lp/static/js/iMAWebCookie.js?4eb9c7dc-1511222ae6f-df4cba773885eb54dfcebd294a039c37&h=www.pages02.net" type="text/javascript"></script>
</body>
</html>
