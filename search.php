<?php get_header(); ?>
	<header class="job-search-header">
		<?php get_template_part('partials/content-breadcrumb'); ?>

		<div class="row">
			<div class="form-wrap">
				<?php get_template_part( 'partials/content', 'register-header' ); ?>

				<?php get_search_form(); ?>
			</div>
		</div><?php /* row */ ?>

	</header>
	<?php /* Not sure exactly what these next 2 lines do. Most likely they close a header-wrap div opened in header.php, but can't be sure.  */ ?>
	<?php if($post->ID == 74 or $post->ID == 61): ?> </div> <?php endif ?>
	</div>

	<section class="page-styles search-results-list">

		<div class="row">
			<div class="small-12 columns">
			
				<h2 class="archive-title">Search Results</h2><p><?php echo $wp_query->found_posts; ?> Listing<?php if($wp_query->found_posts != 1):?>s<?php endif;?></p>

				<?php if (have_posts()) : while (have_posts()) : the_post(); ?>
					<?php get_template_part( 'partials/content', 'job-loop' ); ?>
				<?php endwhile; ?>

				<?php else : ?>

					<article id="post-not-found">

						<header class="article-header">
							<h3><?php _e( 'No jobs found' ); ?></h3>
						</header>
						
						<section class="entry-content">
							<p><?php _e( 'Try searching again' ); ?></p>	
						</section>
					
					</article>

				<?php endif; ?>
	

				<div class="below-nav">
					<?php
					global $wp_query;
					$num_pages = $wp_query->max_num_pages;
					$current = max( 1, get_query_var('paged'));
					$args = array(
						'base'               => '%_%',
						'format'             => '?paged=%#%',
						'total'              => $num_pages,
						'current'            => $current,
						'show_all'           => False,
						'end_size'           => 1,
						'mid_size'           => 2,
						'prev_next'          => True,
						'prev_text'          => __('Prev'),
						'next_text'          => __('Next'),
						'type'               => 'plain',
						'add_args'           => False,
						'add_fragment'       => '',
						'before_page_number' => '',
						'after_page_number'  => ''
					);
					
					if($num_pages>1):
				 		$pagination = paginate_links( $args );
				 		$pagination = str_replace('<a class="next', '<a rel="next" class="next', $pagination);
				 		$pagination = str_replace('<a class="prev', '<a rel="prev" class="prev', $pagination);

				 		echo $pagination;
					endif; 
				?>
				</div>
				<?php /* below-nav */ ?>


			</div><?php /* small-12 */ ?>
		</div><?php /* row */ ?>

	</section>


	<?php get_template_part( 'partials/content', 'trustpilot' ); ?>


<?php get_footer();