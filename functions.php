<?php

// Add custom functions
require_once( 'includes/custom-functions.php' );
// Add jobs expiry  functions
require_once( 'includes/expiry-functions.php' );

// Add theme support woocommerce
add_theme_support( 'woocommerce' );


// Add theme support post thumbnails
add_theme_support('post-thumbnails');


// WP menus
add_theme_support( 'menus' );


// Add image sizes
add_image_size( 'thumbnail', 200, 200, true );
add_image_size( 'image', 700, 350, true );


// Change default excerpt
function new_excerpt_more( $more ) {
	return ' <a class="read-more" href="'. get_permalink( get_the_ID() ) . '">' . __('Read More', 'your-text-domain') . '</a>';
}
add_filter( 'excerpt_more', 'new_excerpt_more' );




function theme_comment($comment, $args, $depth) {
	$GLOBALS['comment'] = $comment; ?>

	<article id="comment-<?php comment_ID() ?>" <?php comment_class(); ?> role="article">
		<header class="article-header bg">
			<h3><?php echo get_comment_author() ?> says&hellip;</h3>
			<?php comment_reply_link(array_merge( $args, array('depth' => $depth, 'max_depth' => $args['max_depth']))) ?>
		</header>

		<section class="entry-content row large-collapse">
			<div class="large-12 columns">
				<?php comment_text() ?>
			</div>
		</section>

	</article>

<?php }



//------------- CON JOB TO DELETE OLD POSTS

// Cron Job to Delete Wishlists over 30 Days
//if(!wp_next_scheduled('remove_old_job_schedule')){
   // wp_schedule_event(time(), 'daily', 'remove_old_job_schedule');
//}

add_action('remove_old_job_schedule', 'cr_remove_old_jobs');

function cr_remove_old_jobs(){

    global $wpdb;

    $date1 = date('Y-m-d', strtotime('2015-06-01 +1 days'));
    $results = $wpdb->get_results ( "SELECT ID FROM wp_posts
                                     WHERE post_date < '".$date1."'
                                     AND post_status = 'publish'");


    error_log(print_r($results,1));
   foreach($results as $post){
        //$current_id = $post->ID;
        $wpdb->query("INSERT INTO `wp_thesmartlist`.`test` (`post_id`) VALUES ('".$post->ID."');");
        $wpdb->query("DELETE FROM `wp_posts` WHERE `ID`='".$post->ID."'");
        $wpdb->query("DELETE FROM `wp_postmeta` WHERE `post_id`='".$post->ID."'");


        // Let the WordPress API do the heavy lifting for cleaning up entire post trails
        //wp_delete_post( $post->ID, true);

    }



}