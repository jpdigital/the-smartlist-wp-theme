<?php /* Template Name: Industry specific */ ?>

<?php get_header(); ?>


	<?php $main_colour = get_field('main_colour'); ?>

	<style type="text/css">
		.color-set{
			color: <?php echo $main_colour; ?> !important;
		}
	</style>


	<?php $src = wp_get_attachment_image_src( get_post_thumbnail_id($post->ID), false, '' ); ?>
	<header class="industry-page-header" style="background-image: url(<?php echo $src[0]; ?>);">
		<div class="logo">
			<img src="<?php echo get_stylesheet_directory_uri(); ?>/library/images/general/sl-logo-white-1.png" class="main-logo" alt="The SmartList" width="160">
		</div>

		<div class="banner-message">
			<h1 class="center caps"><?php the_field('banner_message'); ?></h1>
		</div>

		<div class="arrow left-arrow"></div>
		<div class="arrow right-arrow"></div>
	</header>



	
	<div class="row">
	
		<div class="small-12 large-8 columns" role="main">

			<?php if (have_posts()) : while (have_posts()) : the_post(); ?>

			<article class="main-section" role="article">

				<h2 class="color-set"><?php the_title(); ?></h2>
				<?php the_content(); ?>

			</article>
			
			<?php endwhile; ?>
			
			<?php endif; ?>
			
	
		</div>



		<aside class="small-12 large-4 columns">
		
			<?php $image = wp_get_attachment_image_src(get_field('testimonial_image'), 'thumbnail'); ?>
			<?php if(!empty($image)) : ?><img src="<?php echo $image[0]; ?>" alt="<?php echo get_the_title(get_field('testimonial_image')) ?>" /><?php endif; ?>

			<?php if(get_field('testimonial_name')) : ?><?php the_field('testimonial_name'); ?> says:<?php endif; ?> 

			<?php if(get_field('testimonial')) : ?><blockquote><?php the_field('testimonial'); ?></blockquote><?php endif; ?> 

			<div class="trustpilot-widget" data-locale="en-GB" data-template-id="539ad0ffdec7e10e686debd7" data-businessunit-id="54c641b90000ff00057cfdd6" data-style-height="350px" data-style-width="100%" data-theme="light" data-stars="1,2,3,4,5"></div>

		</aside>


	</div><?php /* row */ ?>

	
	<div class="clear"></div>



	<section class="service-intros row">
		
		<div class="small-12 columns">
			<h1 class="static-border static-border-large">Our Services</h1>
		</div>

		<div class="clear"></div>


		<?php if( have_rows('recruitment_panels') ): $article_count=0; ?>
			<?php while( have_rows('recruitment_panels') ): the_row(); $article_count++; ?>

				<article class="small-12 medium-4 columns article-<?php echo $article_count; ?>">

					<header>
						<div class="service-image">
							<?php $image = wp_get_attachment_image_src(get_sub_field('image'), 'thumbnail'); ?>
							<?php if($image) : ?><img src="<?php echo $image[0]; ?>" alt="<?php echo get_the_title(get_field('image')) ?>" /><?php else: ?>[No image selected]<?php endif; ?>
						</div>

						<div class="request-callback-animated">
							<a href="#callback" class="callback">
								<img src="<?php echo get_stylesheet_directory_uri(); ?>/library/images/general/phone-white-line.png" class="" alt="Phone" width="12">
								<p>Request a callback</p>
							</a>
						</div>

						<?php if(get_sub_field('title')) : ?><h2><?php the_sub_field('title'); ?></h2><?php endif; ?>
					</header>

					<div class="content">
						<?php if(get_sub_field('excerpt')) : ?><p><?php the_sub_field('excerpt'); ?></p><?php endif; ?>
					</div>
					<?php if(get_sub_field('link_url')) : ?><a href="<?php the_sub_field('link_url'); ?>"><button class="learn-more"><?php the_sub_field('link_text'); ?></button></a><?php endif; ?>
				</article>

			<?php endwhile; ?> 
		<?php endif; ?>
	</section><?php /* service-intros */ ?>



	<div class="clear"></div>



	<section class="callback-section" id="callback">
		<div class="row">
			<div class="small-12 columns">

				<div class="standard-form-wrap">

					<div class="form-details">
						<h2 class="static-border">Request a Callback</h2>
						<p>We'll call you. Just give us a few details and let us know what you're interested in.</p>
						<p>We'll then arrange for an advisor to get in touch with you straight away * and fill you in on exactly what you need to know.</p>
						<p>Nice and easy.</p>
						<p><small>* We're able to call between 9am - 5pm</small></p>
					</div>

					<?php 
				    $form = get_field('form_to_display');
				    gravity_form_enqueue_scripts($form->id, true);
				    gravity_form($form->id, false, false, false, '', true, 1); 
					?>

					<div class="clear"></div>
				</div><?php /* form-wrap */ ?>

			</div>
		</div><?php /* row */ ?>
	</section>

		

<?php get_footer();