<?php get_header(); ?>

<!-- START: Breadcrumb -->
<?php get_template_part( 'partials/content', 'breadcrumb' ); ?>
<!-- END: Breadcrumb -->
	<section class="articles">
	<div class="row">
		<div class="small-12 medium-12 " role="main">

			<?php $i = 0;  if (have_posts()) : while (have_posts()) : the_post(); $i++ ?>

			<?php if($i == 1) : ?>
				<?php get_template_part( 'partials/content', 'article-loop-large' ); ?>

			<?php else : ?>
				<?php if($i==2): ?><section  class="row"><?php endif ?>

				<?php get_template_part( 'partials/content', 'article-loop' ); ?>

			<?php endif; ?>
			
			<?php endwhile; ?>
				</section>
			<?php else : ?>
			
			<article class="post-not-found">
				
				<header class="not-found-header">
					
					<h1><?php _e( 'Nothing Found!' ); ?></h1>
				
				</header>
				
				<section class="not-found-content">
					
					<p><?php _e( 'Please check what you are looking for.' ); ?></p>
				
				</section>
			
			</article>
			
			<?php endif; ?>
			
			<div class="below-nav">
				
				<?php posts_nav_link(' - ', '&laquo; Prev', 'Next &raquo;'); ?>
			
			</div>
		
		</div>
		

	</div>
</section>
<section class="latest-resources">
	<div class="row">
		
			<?php get_recent(3,null, 'post', 'article-loop','category',9, TRUE, 'recent-fallback'); ?>
	
	</div>
</section>
<?php get_footer();