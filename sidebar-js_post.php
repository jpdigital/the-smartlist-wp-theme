<div id="main-sidebar" class="sidebar" role="complementary">
	
	<?php get_recent(3 ,'Recent Jobseekers Articles', 'js_post' ) ?>

	<?php get_most_commented(3,'Most popular','js_post','insights-short'); ?>

	<?php if ( is_active_sidebar( 'Main Sidebar' ) ) : ?>
	
		<?php dynamic_sidebar( 'main-sidebar' ); ?>
	
	<?php else : ?>
	
	<div class="no-widgets">
		
		<p><?php _e( 'Please add some widgets.' );  ?></p>
	
	</div>
	
	<?php endif; ?>

</div>

