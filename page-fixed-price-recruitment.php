<?php /* Template Name: Fixed Price Recruitment */ ?>

<?php get_header();

$fpr_fields = get_fields();

$fpr_hero_heading = $fpr_fields['hero_section_heading'];
$fpr_hero_video_embed = $fpr_fields['hero_video_embed_link'];
$fpr_trustpilot_img = wp_get_attachment_image_src($fpr_fields['hero_trustpilot_image'], 'full');

?>

	<?php get_template_part('partials/content-breadcrumb'); ?>

	<?php if (have_posts()) : while (have_posts()) : the_post(); ?>

		<section class="hero-section">
			<div class="row">

				<div class="small-12 large-6 columns">

					<div class="hero-content-wrap">
						<?php
							if ($fpr_hero_heading) : echo '<h1 class="caps">'.$fpr_hero_heading.'</h1>'; endif;
							the_content();
						?>

						<a href="#callback" class="scroll">
							<button class="button-request-callback">
								<img src="/wp-content/themes/thesmartlist/library/images/general/phone-white-line.png" class="" alt="Phone" width="12">
								Request a callback
							</button>
						</a>

						<?php echo '<img src="'.$fpr_trustpilot_img[0].'" alt="'.get_the_title($fpr_fields['hero_trustpilot_image']).'" class="hero-tp" />'; ?>
					</div>

				</div>

				<?php if ($fpr_hero_video_embed) :
					echo '<div class="small-12 large-6 columns">
						<div class="hero-video-wrap">
							<div class="video-wrapper">
								'.$fpr_hero_video_embed.'
							</div>
						</div>
					</div>';
				endif; ?>

			</div>

			<div class="row mob-details">
				<div class="small-12 columns">
					<h3>
						Call us today on <br>
						<strong><?php the_field('tel_eng','options'); ?></strong> or
					</h3>

					<a href="#callback" class="scroll">
						<button class="button-request-callback">
							<img src="/wp-content/themes/thesmartlist/library/images/general/phone-white-line.png" class="" alt="Phone" width="12">
							Request a callback
						</button>
					</a>
				</div>
			</div>
		</section>

</div><?php /* header-wrap - Opened in header.php*/ ?>


		<?php get_template_part( 'partials/content', 'trustpilot' ); ?>


 		<section class="packages-section">
			<?php
			$fpr_packages = $fpr_fields['our_packages'];

			if( $fpr_packages ):

				echo '<div class="row" data-equalizer>

	 				<div class="small-12 columns">
	 					<h1 class="caps">Our Packages</h1>
	 				</div>';

					$counter = 0;
					foreach( $fpr_packages as $field_name => $value ):

						//http://www.advancedcustomfields.com/resources/get_fields/
						// get_field_object( $field_name, $post_id, $options )
						// - $value has already been loaded for us, no point to load it again in the get_field_object function
						$field = get_field_object($field_name, false, array('load_value' => false));

						//Create a class name out of Title
						$package_title_class = str_replace(" ", "-", $value['package_title']);
						$package_title_class = strtolower($package_title_class);

						//Set class differences for boxes
						if ($counter == 0) :
							$col_classes = 'small-12';
							$box_classes = 'package-box package-box-first';
						else:
							$col_classes = 'small-12 large-6';
							$box_classes = 'package-box';
							$equalizer = 'data-equalizer-watch';
						endif;

						//Get fields
						$package_icon = wp_get_attachment_image_src($value['package_icon'], 'full');
						$package_promo = wp_get_attachment_image_src($value['package_promo_image'], 'full');


						echo '<div class="'.$col_classes.' columns">
		 					<div class="'.$box_classes.' '.$package_title_class.'-bg-colour"'.$equalizer.'>

		 						<div class="package-box-content">';

									if( $package_icon ) :
										echo '<img src="'.$package_icon[0].'" alt="'.get_the_title($value['package_icon']).'" class="package-icon"/>';
									endif;

									echo '<h2 class="caps">'.$value['package_title'].'</h2>
				 					<p><strong>'.$value['package_short_description'].'</strong></p>

				 					<label><strong>What you get...</strong></label>
				 					'.$value['package_list'].'

									<a href="#callback" class="scroll">
										<button class="button-request-callback">
											<img src="/wp-content/themes/thesmartlist/library/images/general/phone-white-line.png" class="" alt="Phone" width="12">
											Request a callback
										</button>
									</a>
		 						</div>';

								if( $package_promo ) :
		 							echo '<div class="package-box-img">
		 								<img src="'.$package_promo[0].'" alt="'.get_the_title($value['package_promo_image']).'"/>
		 							</div>';
								endif;

		 						echo '<div class="clearfix"></div>

		 					</div>
		 				</div>';
						$counter++;
					endforeach;

				echo '</div>';

			endif; ?>
 		</section>

		<section class="pricing-section">
			<?php
			$fpr_pricing = $fpr_fields['pricing_table'];
			if( $fpr_pricing ):

				echo '<div class="row" data-equalizer>

					<div class="small-12 large-4 columns desktop-feature-list">

						<div class="feautre-list-header" data-equalizer-watch>
							<h1 class="caps center">Simple Pricing</h1>
						</div>

						<ul>';
						$labels = array();
						foreach( $fpr_pricing as $field_name => $value ):

							//http://www.advancedcustomfields.com/resources/get_fields/
							// get_field_object( $field_name, $post_id, $options )
							// - $value has already been loaded for us, no point to load it again in the get_field_object function
							$field = get_field_object($field_name, false, array('load_value' => false));

							echo '<li><strong>'.$value['pricing_feature'].'</strong></li>';
							$labels[] = $value['pricing_feature'];
						endforeach;

						echo '</ul>
					</div>';


					echo '<div class="small-12 large-8 columns">
						<div class="row">';

							$col = array('','','');
							$counter = 0;
							$fields = array('smartlist_fixed','smartlist_search','smartlist_post');

							foreach( $fpr_pricing as $field_name => $value ):

								$field = get_field_object($field_name, false, array('load_value' => false));

								$fpr_feature = $value['feature_included'];
								$li_open = '<li><span class="feature-name">'.$labels[$counter].'</span>';
								$li_close = '</li>';

								foreach( $fpr_feature as $field_name => $value ):

									foreach ($fields as $key=>$val) :

										if ($value[$val] == 1) :
											$col[$key] .= $li_open.'<span class="icon icon-tick"></span>'.$li_close;
										else:
											$col[$key] .= $li_open.'<span class="icon icon-cross"></span>'.$li_close;
										endif;

									endforeach;

								endforeach;

								$counter++;

							endforeach;

							$fixed_package_icon = wp_get_attachment_image_src($fpr_fields['fixed_package_icon'], 'full');
							$search_package_icon = wp_get_attachment_image_src($fpr_fields['search_package_icon'], 'full');
							$post_package_icon = wp_get_attachment_image_src($fpr_fields['post_package_icon'], 'full');

							$header_box = array(
								'<div class="package-header-wrap" data-equalizer-watch>
									<div class="t-cell">

										<header class="smartlist-fixed-bg-colour caps">
											<div class="header-img">
												<img src="'.$fixed_package_icon[0].'" alt="'.get_the_title($fpr_fields['fixed_package_icon']).'"/>
											</div>
											<div class="header-content">
												<h2>Gold</h2>
												<p class="caps">'.$fpr_fields['fixed_package_price'].'</p>
											</div>
											<div class="clearfix"></div>
										</header>

									</div>
								</div>',

								'<div class="package-header-wrap" data-equalizer-watch>
									<div class="t-cell">

										<header class="smartlist-search-bg-colour caps">
											<div class="header-img">
												<img src="'.$search_package_icon[0].'" alt="'.get_the_title($fpr_fields['search_package_icon']).'"/>
											</div>
											<div class="header-content">
												<h2>Silver</h2>
												<p class="caps">'.$fpr_fields['search_package_price'].'</p>
											</div>
											<div class="clearfix"></div>
										</header>

									</div>
								</div>',

								'<div class="package-header-wrap" data-equalizer-watch>
									<div class="t-cell">

										<header class="smartlist-post-bg-colour caps">
											<div class="header-img">
												<img src="'.$post_package_icon[0].'" alt="'.get_the_title($fpr_fields['post_package_icon']).'"/>
											</div>
											<div class="header-content">
												<h2>Bronze</h2>
												<p class="caps">'.$fpr_fields['post_package_price'].'</p>
											</div>
											<div class="clearfix"></div>
										</header>

									</div>
								</div>'
							);

							echo '<div class="small-12 large-4 columns package-wrap">'
								.$header_box[0];
								if ($fpr_fields['fixed_package_promoted']) : echo '<div class="package-promoted"></div>'; endif;
								echo '<ul class="smartlist-fixed">'
									.$col[0]
								.'</ul>
							</div>';

							echo '<div class="small-12 large-4  columns package-wrap">'
								.$header_box[1];
								if ($fpr_fields['search_package_promoted']) : echo '<div class="package-promoted"></div>'; endif;
								echo '<ul class="smartlist-search">'
									.$col[1]
								.'</ul>
							</div>';

							echo '<div class="small-12 large-4 columns package-wrap">'
								.$header_box[2];
								if ($fpr_fields['post_package_promoted']) : echo '<div class="package-promoted"></div>'; endif;
								echo '<ul class="smartlist-post">'
									.$col[2]
								.'</ul>
							</div>';

						echo '</div>';

					echo '</div>';

				echo '</div>';
				//End row
			endif; ?>

			<div class="row">
				<div class="small-12 large-8 large-offset-4 columns">
					<a href="#callback" class="scroll">
						<button class="button-request-callback">
							<img src="/wp-content/themes/thesmartlist/library/images/general/phone-white-line.png" class="" alt="Phone" width="12">
							Request a callback
						</button>
					</a>
				</div>
			</div>
		</section>

		<section class="page-styles callback-section" id="callback">
			<div class="row">
				<div class="small-12 columns">

					<div class="standard-form-wrap">

						<div class="form-details">
							<h2 class="static-border">Request a Callback</h2>
							<p>Find out how we can help with a free callback. Just leave a few details and a good time to call, and we'll be in touch.</p>

							<hr>

							<ul class="call-today">
								<li><h3><strong>Call today</strong></h3></li>
								<li><p>T. <?php the_field('tel_eng','options'); ?></p></li>
								<li><p>Give us a ring and we'll chat through your plans and offer some expert advice.</p></li>
							</ul>
							<div class="button-pos">
								<button class="caps"><a href="https://www.jobstoday.co.uk/employers/796588-smart-hire">Jobseekers click here</a></button>
							</div>
						</div>

						<?php
					    $form = get_field('form_to_display');
					    gravity_form_enqueue_scripts($form->id, true);
					    gravity_form($form->id, false, false, false, '', true, 1);
						?>

						<div class="clear"></div>
					</div><?php /* form-wrap */ ?>

				</div>
			</div><?php /* row */ ?>
		</section>

	<?php endwhile; ?>

	<?php else : ?>
	<?php endif; ?>

<?php get_footer();