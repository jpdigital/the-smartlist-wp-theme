<?php get_header(); ?>

<div class="row page-styles">
	<div class="small-12 columns" role="main">

		<article class="post-not-found">
			<h1><?php _e( 'Oops...' ); ?></h1>
			<p><?php _e( "We can't find the page you're looking for." ); ?></p>
			<p><?php _e( "Either the page no longer exists, or it's been moved." ); ?></p>
			<p>
				<?php _e( "If you need help, please don't hesitate to" ); ?>
				<a href="<?php echo site_url('contact'); ?>"><?php _e( "get in touch" ); ?></a>
				<?php _e( "with us." ); ?>
			</p>
			<p><?php _e( "We're here to help." ); ?></p>

			<br>
			<p><strong><?php _e( "Jobseekers" ); ?></strong></p>
			<p>
				<?php _e( "If you're looking for employment, check out our" ); ?>
				<a href="<?php echo site_url('jobseekers'); ?>"><?php _e( "Jobseekers" ); ?></a>
				<?php _e( "page." ); ?></p>
				<p><?php _e( "It's updated reguarly, so check back often." ); ?></p>
		</article>

	</div>
</div><?php /* row */ ?>

<?php get_footer();