<?php 

/* Template Name: Import Delete  */

get_header(); ?>

<?php
	$args = array(

			'post_type'			=>	'job',
			'post_status'		=>	'publish',
			'meta_query'		=>	array(
				array(
					'key'     => 'job_priority',
					'value'   => 1,
					'compare' => '=',
				),
			),
	);

	$priOneJobs = new WP_Query($args);

		
	if(count($priOneJobs->found_posts) > 0) {
		echo '<br>Have Jobs to update: '.$priOneJobs->found_posts;

		if($priOneJobs->have_posts()): 
			while($priOneJobs->have_posts()): 
				$priOneJobs->the_post();

				//var_dump($post);
				//	Grab meta for this job
				$postMeta = get_post_meta( $post->ID );
				//	We need these fields:
				$advertStart = new DateTime($postMeta['advert_start_date'][0]); // yyyymmdd
				$weeksToAdvertise = $postMeta['advertise_weeks'][0];
				$daysToAdvertise = $weeksToAdvertise*7;
				$advertEnd = $postMeta['advert_end_date'][0];
				$advertActive = $postMeta['advert_active'][0];
				$today = new DateTime('now');

				$dateInt = 'P'.$daysToAdvertise.'D';
				//	Initialise our new End to be the same date as the advert start - as we'll add the ad period onto it.
				$advertNewEnd = new DateTime($advertStart->format('Ymd'));

				//Add the nuber of days to advertise onto the advert start date
				$advertNewEnd->add(new DateInterval($dateInt));

				//	Now update the end date
				$advertEnd = $advertNewEnd;
				
				//	If the end date is before today then we need to expire the job.
				if($advertEnd < $today):
					update_post_meta($post->ID, 'advert_active', 0);
				endif;
			endwhile;
		endif;
	}
	else {
		echo 'No LogicMelon jobs to update';	
	};
?>

	<?php if (is_page(39)){
		//About page conditional check
		$classes = 'about-page-wrap';
	}?>

	<div class="sl-logo-notype">
		<?php /* Background image */ ?>
		<img src="<?php echo get_stylesheet_directory_uri(); ?>/library/images/sl-logo-colour-1-notype.png" alt="" width="320px">
	</div>

	<!-- START: Breadcrumb -->
	<?php get_template_part( 'partials/content', 'breadcrumb' ); ?>
	<!-- END: Breadcrumb -->

	<div class="row page-styles <?php if(isset($classes)){echo $classes;}?>">
	
		<div class="small-12 medium-10 large-8 columns" role="main">

			<?php if (have_posts()) : while (have_posts()) : the_post(); ?>

				<article id="post-<?php the_ID(); ?>" <?php post_class(); ?> role="article">
					
					<header class="article-header">
						
						<h1 class="static-border static-border-large"><?php the_title(); ?></h1>
					
					</header>
					
					<section class="entry-content">
						
						<?php if($post->post_content=="") : ?>
							<p>There's nothing here right now. </p>
							<p>We're likely just updating this page. Check back later.</p>
						<?php else : ?>
							<?php the_content(); ?>
						<?php endif; ?>

					</section>
					
				</article>
			
			<?php endwhile; ?>
			
			<?php else : ?>
			
				<article class="post-not-found">
					
					<header class="not-found-header">
						
						<h2><?php _e( 'Nothing Found!' ); ?></h2>
					
					</header>
					
					<section class="not-found-content">
						
						<p><?php _e( 'Please check what you are looking for' ); ?></p>
					
					</section>
				
				</article>
			
			<?php endif; ?>
		</div>

		<?php
		$facts = get_field('facts_box');
		$get_in_touch = get_field('get_in_touch_box');
		?>


		<?php if(!empty($facts) || !empty($get_in_touch)): ?>

			<aside class="small-12 medium-12 large-4 columns">

				<?php if(!empty($facts)): ?>
					<div class="aside-content">
						<h2 class="static-border">Facts</h2>
						<?php the_field('facts_box'); ?>
					</div>
				<?php endif; ?>

				<?php if(!empty($get_in_touch)): ?>
					<div class="aside-content get-in-touch">
						<h2 class="static-border">Get in touch</h2>
						<?php the_field('get_in_touch_box'); ?>
					</div>
				<?php endif; ?>


				<button class="button-request-callback">
					<img src="<?php echo get_stylesheet_directory_uri(); ?>/library/images/general/phone-white-line.png" class="" alt="Phone" width="12">
					Request a callback
				</button>

			</aside>

		<?php endif; ?>

	</div><?php /* row */ ?>




	<!-- START: Trustpilot -->
	<?php get_template_part( 'partials/content', 'trustpilot' ); ?>
	<!-- END: Trustpilot -->

<?php get_footer();