<!doctype html>

<!--[if lt IE 7]><html <?php language_attributes(); ?> class="no-js lt-ie9 lt-ie8 lt-ie7"><![endif]-->
<!--[if (IE 7)&!(IEMobile)]><html <?php language_attributes(); ?> class="no-js lt-ie9 lt-ie8"><![endif]-->
<!--[if (IE 8)&!(IEMobile)]><html <?php language_attributes(); ?> class="no-js lt-ie9"><![endif]-->
<!--[if (IE 9)&!(IEMobile)]><html <?php language_attributes(); ?> class="no-js lt-ie10"><![endif]-->
<!--[if gt IE 8]><!--> <html <?php language_attributes(); ?> class="no-js"><!--<![endif]-->

<head>
	<meta charset="utf-8">

	<title><?php wp_title('-'); ?></title>

	<meta name="HandheldFriendly" content="True">
	<meta name="MobileOptimized" content="320">
	<meta name="viewport" content="width=device-width, initial-scale=1.0"/>

	<link rel="apple-touch-icon" href="<?php echo get_template_directory_uri(); ?>/library/images/apple-icon-touch.png">
	<link rel="shortcut icon" href="<?php echo get_stylesheet_directory_uri(); ?>/favicon.png" />

	<!--[if IE]>
		<link rel="shortcut icon" href="<?php echo get_template_directory_uri(); ?>/favicon.ico">
	<![endif]-->
	<?php // set /favicon.ico for IE10 win ?>
	<meta name="msapplication-TileColor" content="#d3492f">
	<meta name="msapplication-TileImage" content="<?php echo get_template_directory_uri(); ?>/library/images/win8-tile-icon.png">
  <meta name="com.silverpop.brandeddomains" content="www.pages02.net,www.digitalkitbag.com,www.jobstoday.co.uk,www.thesmartlist.co.uk" />

	<link rel="pingback" href="<?php bloginfo('pingback_url'); ?>">

	<?php wp_head(); ?>

	<!-- Google Tag Manager -->
	<noscript><iframe src="//www.googletagmanager.com/ns.html?id=GTM-MRNG3V"
	height="0" width="0" style="display:none;visibility:hidden"></iframe></noscript>
	<script>(function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
	new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
	j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
	'//www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
	})(window,document,'script','dataLayer','GTM-MRNG3V');</script>
	<!-- End Google Tag Manager -->
	<!-- Coremetrics Tag -->
	<script type="text/javascript" src="//nexus.ensighten.com/johnstonpress/production/Bootstrap.js"></script>
	<!-- End Coremetrics Tag -->

</head>
<body <?php body_class(); ?>>

	<?php if(is_page_template('page-jobseekers.php') || is_post_type_archive('job') || is_page_template('single-job.php') || is_page_template('page-landing.php') || is_page_template('page-fixed-price-recruitment.php')) : ?>
		<div class="header-wrap">
	<?php endif ?>


			<?php if(is_page_template('page-industry-specific.php') || is_page_template('page-industry-specific-alt.php')) : ?>

			<?php else : ?>
		
				<header class="page-header">
					
						<div class="recruiter-bar">
							<div class="row">
								<div class="small-12 columns">
									<div class="trustpilot-wrap">
	
										<?php 
										//Check if page is home and set a variable that affects the theme of the Trustpilot embed in the recruiter bar
										if(is_page('home')) :
											$theme = "dark";
										else :
											//$theme = "light";
											$theme = "dark";
										endif; ?>
	
<!-- 										<div class="trustpilot-widget" data-locale="en-GB" data-template-id="5419b732fbfb950b10de65e5" data-businessunit-id="54c641b90000ff00057cfdd6" data-style-height="20px" data-style-width="100%" data-theme="<?php echo $theme; ?>"></div> -->
									</div>
	
									<a href="<?php the_field('recruiter_login_url','options'); ?>" target="_blank" class="login">
										<button class="empty-rounded caps">
											Recruiter Login
										</button>
									</a>
									
									<a href="tel:<?php the_field('tel_eng','options'); ?>" class="call">
										<button class="button-request-callback">
											<img src="<?php echo get_stylesheet_directory_uri(); ?>/library/images/general/phone-white-line.png" class="" alt="Phone" width="12">Call us now
										</button>
									</a>
									
									<div class="clear"></div>
								</div>
							</div>
						</div>
						

						<nav class="main-bar">
							<div class="row">
								<div class="small-12 columns">

									<div class="logo-wrap">
										<a href="<?php echo home_url(); ?>" rel="nofollow">
											<img src="<?php echo get_stylesheet_directory_uri(); ?>/library/images/general/sl-logo-colour-1.png" class="main-logo" alt="The SmartList">
											<img src="<?php echo get_stylesheet_directory_uri(); ?>/library/images/general/sl-logo-white-1.png" class="main-logo-white" alt="The SmartList">
										</a>
									</div>
							
									<div class="menu-wrap">
										<?php wp_nav_menu( array(
											'theme_location' => 'main-menu',
											'menu_id'=> 'main-menu',
											'container' => 'nav',
											'container_class' => '',
											'after' => '<div class="mobile-dropdown-trigger"></div>' ) ); ?>
									</div>

									<div class="contact-wrap">
										<h1><strong>Call us today on<br><?php the_field('tel_eng','options'); ?></strong></h1>
										<p>or <a href="<?php echo site_url('contact'); ?>">Request a callback</a></p>
									</div>

								</div><?php /*small-12*/ ?>

							</div><?php /*row*/ ?>

							<div class="clear"></div>
						</nav>

						<div class="mobile-menu-trigger">
							<hr class="top"><hr class="middle"><hr class="bottom">
						</div>


				</header>


			<?php endif; ?>