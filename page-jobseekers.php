<?php
/* Template Name: Job Seekers  */
get_header(); ?>

	<?php if (have_posts()) : while (have_posts()) : the_post(); ?>

		<article id="post-<?php the_ID(); ?>" <?php post_class(); ?> role="article">

			<header class="job-search-header">

				<?php get_template_part('partials/content-breadcrumb-cv'); ?>



				<div class="row">
					<div class="form-wrap">
						<?php get_template_part( 'partials/content', 'register-header' ); ?>

						<?php get_search_form(); ?>
					</div>
				</div><?php /* row */ ?>

			</header>

			<?php if($post->ID == 74 or $post->ID == 61): ?> </div> <?php endif ?>

				<!-- START: Featured Videos -->
	            <?php get_template_part( 'partials/content', 'featured-videos' ); ?>
	            <!-- END: Featured Videos -->

				<!-- START: Intro Content -->
				<section class="page-styles section entry-content proposition">
					<div class="row">
						<div class="large-12 columns">
							<?php if(get_field('main_content_title')) : ?><h2 class="static-border static-border-large"><?php the_field('main_content_title'); ?></h2><?php endif; ?>
						</div>

						<div class="large-8 columns">
							<?php if(get_field('main_content_copy')) : ?><?php the_field('main_content_copy'); ?><?php endif; ?>
						</div>

						<div class="large-4 columns tick-list">
							<?php if(get_field('reasons')) : ?><?php the_field('reasons'); ?><?php endif; ?>
						</div>
				</div><?php /* row */ ?>
			</section>
			<!-- END: Intro Content -->

            <!-- START: Featured Videos -->
            <?php //get_template_part( 'partials/content', 'featured-videos' ); ?>
            <!-- END: Featured Videos -->

			<!-- START: Featured Jobs -->
			<?php get_template_part( 'partials/content', 'featured-jobs' ); ?>
			<!-- END: Featured Jobs -->


		</article>


	<?php endwhile; ?>
	<?php endif; ?>

	<?php get_template_part( 'partials/content', 'trustpilot' ); ?>

<?php get_footer();