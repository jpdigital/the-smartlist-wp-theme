<?php
/*
Insights template
This is the template used for the Posts (Insights) Archive apge
*/
get_header(); ?>


<div class="row">
	<div class="small-12 columns insights-subscribe-cta">
		<a href="#subscribe" class="scroll"><button><span>&darr;</span> &nbsp; Subscribe to our Recruiter Newsletter &nbsp; <span>&darr;</span></button></a>
	</div>
</div>


<!-- START: Breadcrumb -->
<?php get_template_part( 'partials/content', 'breadcrumb' ); ?>
<!-- END: Breadcrumb -->

<section class="articles page-styles">

	<div class="row">
		<div class="small-12 medium-12 columns" role="main">

			<?php
			$args = array(
				'category__not_in' => 9
			);
			$latest_insights = new WP_Query( $args ); ?>
			
			<?php $i = 0; if ($latest_insights->have_posts()) : $c = 1; while ($latest_insights->have_posts()) : $latest_insights->the_post(); $i++; $c++;?>
				
				<?php if($i == 1) : ?>
				<div class="row">
					<?php get_template_part( 'partials/content', 'article-loop-large' ); ?>
				</div>

				<?php else : ?>
					<?php if($i==2): ?>
						<section class="row">
					<?php endif ?>

						<?php include(locate_template('partials/content-article-loop.php')); ?>

				<?php endif; ?>
			
			<?php endwhile; else : ?>


				<p class="no-posts center">There aren't any Insights right now. <br> Check back soon.</p>


				</section><?php /* row */ ?>
			
			<?php endif; ?>
			
			<?php wp_reset_query(); ?>
			
		</div>
	</div><?php /* row */ ?>

</section><?php /* articles */ ?>



<section class="latest-resources">
	<div class="row">
		<div class="small-12 columns">
			<?php get_recent(3,null, 'post', 'article-loop','category',9, TRUE, 'recent-fallback'); ?>
		</div>
	</div>
</section>

<?php get_template_part( 'partials/content', 'trustpilot' ); ?> 

<?php get_footer();