<?php /* Template Name: Contact */ ?>

<?php get_header(); ?>

	<div class="sl-logo-notype">
		<?php /* Background image */ ?>
		<img src="<?php echo get_stylesheet_directory_uri(); ?>/library/images/sl-logo-colour-1-notype.png" alt="" width="320px">
	</div>

	<?php get_template_part( 'partials/content', 'breadcrumb' ); ?>

	<div class="row page-styles">

		<div class="small-12 columns" role="main">

			<?php if (have_posts()) : while (have_posts()) : the_post(); ?>

			<article id="post-<?php the_ID(); ?>" <?php post_class(); ?> role="article">

				<header class="article-header">

					<h1 class="static-border static-border-large"><?php the_title(); ?></h1>

				</header>


				<section>

					<div class="standard-form-wrap">

						<div class="form-details">
							<h2 class="static-border">We're here to help</h2>
							<?php the_content(); ?>
						</div>

						<?php
							$form = get_field('form_to_display');
							gravity_form_enqueue_scripts($form->id, true);
							//If the Ajax parameter is set to true (second from last) then the dropdown loses its style if the form fails to submit
							gravity_form($form->id, false, false, false, '', true, 1);
						?>
					</div>


				</section>

			</article>

			<?php endwhile; else : ?>
			<?php endif; ?>

		</div>

	</div><?php /* row */ ?>

	<!-- START: Trustpilot -->
	<?php get_template_part( 'partials/content', 'trustpilot' ); ?>
	<!-- END: Trustpilot -->

<?php get_footer();