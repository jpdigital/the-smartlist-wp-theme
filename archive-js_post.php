<?php
 get_header(); ?>

<!-- START: Breadcrumb -->
<?php get_template_part( 'partials/content', 'breadcrumb' ); ?>
<!-- END: Breadcrumb -->

<div class="articles page-styles">

	<div class="row">
	
		<div class="small-12 medium-12 columns" role="main">
			<div class="row">

			<?php $i = 0; if (have_posts()) : $c = 1; while (have_posts()) : the_post(); $i++; $c++;?>

				<?php if($i == 1) : ?>
					<?php get_template_part( 'partials/content', 'article-loop-large' ); ?>

				<?php else : ?>
					<?php include(locate_template('partials/content-article-loop.php')); ?>

				<?php endif; ?>

			<?php endwhile; else : ?>

				<p class="no-posts center">There aren't any Articles right now. <br> Check back soon.</p>

			<?php endif; ?>
			</div>
		</div>
		

	</div><?php /* row */ ?>
		
</div>

<?php get_template_part( 'partials/content', 'trustpilot' ); ?> 

<?php get_footer();