# The Smartlist Custom Theme

The theme was originally created by First10 to support manual job entry and granular meta search queries.
Since the website was switched to data import via WP ALL Import the missing meta data (e.g. salary range) caused a zero search results issue.
This has been fixed in version 1.1 

### Prerequisites

Minimum requirements for job search to work is a active jobs in the database with Location and Parent location defined in wp_postmeta

### Installing

Please follow the standard installation procedure from Bitbucket Repo

## Authors
* First10
* Jack Harper / JPIMedia Ltd

<jack.harper@jpimedia.co.uk>

## Version
* 1.1 Removed search by salary and job type to fix zero search results
* 1.0 Initial setup by First10
