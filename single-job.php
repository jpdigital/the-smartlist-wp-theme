<?php
/*
 * Single Job Post
 */
get_header(); ?>

<div class="sl-logo-notype">
	<?php /* Background image */ ?>
	<img src="<?php echo get_stylesheet_directory_uri(); ?>/library/images/sl-logo-colour-1-notype.png" alt="" width="320px">
</div>

<?php get_template_part( 'partials/content', 'breadcrumb' ); ?>

<div class="row">
	<div class="small-12 large-8 columns page-styles job-details" role="main">

		<?php while (have_posts()) : the_post(); ?>
			<?php
				//$jobMeta = get_post_meta($post->ID);
				//var_dump($jobMeta);
			?>
			<header>
				<h1><?php the_title(); ?></h1>
			</header>

			<?php if(get_field('location')) : ?><p><strong>Location</strong>: <?php the_field('location'); ?></p><?php endif; ?>
			<?php if(get_field('salary')) : ?><p><strong>Salary</strong>:
				<?php
					//	Output a £ symbol if the first character is a numebr.
					if(is_numeric(substr(get_field('salary'), 0, 1))):
						echo '&pound;';
					endif;
					the_field('salary');
				?>
				</p><?php endif; ?>
			<?php if(get_field('contract_type')) : ?><p><strong>Hours</strong>: <?php the_field('contract_type'); ?></p><?php endif; ?>

			<br>
			<?php
				$contentStr = str_replace('&nbsp;', '',get_the_content());
				$contentStr = str_replace('&pound;', '£',$contentStr);
				echo html_entity_decode($contentStr);
			?>


			<?php if(get_field('url')) : ?>
				<div class="large-apply">
					<a href="<?php the_field('url'); ?>" class="apply-btn">Apply for this position</a>
				</div>
			<?php endif; ?>

		<?php endwhile; ?>

	</div>

	<div class="small-12 large-4 columns page-styles">
		<?php get_sidebar('job'); ?>
	</div>

</div><?php /* row */ ?>


<?php get_template_part( 'partials/content', 'trustpilot' ); ?>


<?php get_footer();