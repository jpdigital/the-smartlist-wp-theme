<?php /* Template Name: Sub Landing Page */ ?>

<?php get_header(); ?>
	
<!-- START: Breadcrumb -->
<?php get_template_part( 'partials/content', 'breadcrumb' ); ?>
<!-- END: Breadcrumb -->

	<div class="row">
	
		<div class="small-12 columns" role="main">

			<?php if (have_posts()) : while (have_posts()) : the_post(); ?>

			<article id="post-<?php the_ID(); ?>" <?php post_class(); ?> role="article">


				
				<header class="article-header">

					<!-- START: Banner Content -->
					
					<?php $image = wp_get_attachment_image_src(get_field('banner_img'), 'full'); ?>
					<?php if($image) : ?><img src="<?php echo $image[0]; ?>" alt="<?php echo get_the_title(get_field('banner_img')) ?>" /><?php else: ?>[No banner image selected]<?php endif; ?>

					<h1>
						
						<a href="<?php the_permalink() ?>" rel="bookmark" title="<?php the_title_attribute(); ?>"><?php the_title(); ?></a>
						
					</h1>
					<?php the_content(); ?> 

					<!-- END: Banner Content -->
				
				</header>

				
				<!-- START: Service Panels -->
				
				<?php if( have_rows('panels') ): $i = 0; ?>
				    <?php while( have_rows('panels') ): the_row(); $i++; ?>
				 	
				       <section id="service-<?php echo $i; ?>" class="entry-content <?php if (($i % 2)==0) : ?>even<?php else : ?>odd<?php endif; ?>">
					
							<?php if(get_sub_field('title')) : ?><h2><?php the_sub_field('title'); ?></h2><?php endif; ?>

							<?php if(get_sub_field('thumbnail')) : ?>
								<?php $image = wp_get_attachment_image_src(get_field('thumbnail'), 'thumbnail'); ?>
								<img src="<?php echo $image[0]; ?>" alt="<?php echo get_the_title(get_field('thumbnail')) ?>" />
							<?php endif; ?>
							<?php if(get_sub_field('content')) : ?><span class="content"><?php the_sub_field('content'); ?></span><?php endif; ?>
						
						</section>
				        
				    <?php endwhile; ?> 
				<?php endif; ?>

				<!-- END: Service Panels -->
				
				<!-- START: Callback Block -->
				<section id="callback">
				<?php 
				    $form = get_field('form_to_display');
				    gravity_form_enqueue_scripts($form->id, true);
				    gravity_form($form->id, true, true, false, '', true, 1); 
				?>
				</section>
				<!-- END: Callback Block -->

			</article>
			
			<?php endwhile; ?>
			
			<?php else : ?>
			
			<article class="post-not-found">
				
				<header class="not-found-header">
					
					<h2><?php _e( 'Nothing Found!' ); ?></h2>
				
				</header>
				
				<section class="not-found-content">
					
					<p><?php _e( 'Please check what you are looking for.' ); ?></p>
				
				</section>
			
			</article>
			
			<?php endif; ?>
			
			<div class="below-nav">
				
				<?php posts_nav_link(' - ', '&laquo; Prev', 'Next &raquo;'); ?>
			
			</div>
		
		</div>
		
	</div>

	<!-- START: Trustpilot -->
	<?php get_template_part( 'partials/content', 'trustpilot' ); ?> 
	<!-- END: Trustpilot -->

<?php get_footer();